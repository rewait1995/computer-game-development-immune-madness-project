#ifndef _AMMO2_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _AMMO2_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"

class Ammo2ParticleManager
{
	private:
	Particle particles[MAX_NUMBER_AMMO_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float Ammo2ParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	Ammo2ParticleManager();
	~Ammo2ParticleManager();
	void Ammo2ParticleManager::setInvisibleAllParticles();
	void Ammo2ParticleManager::setVisibleNParticles(int n);
	void Ammo2ParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void Ammo2ParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool Ammo2ParticleManager::initialize(Graphics *g);

	void Ammo2ParticleManager::update(float frametime);
	void Ammo2ParticleManager::draw();
	void Ammo2ParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
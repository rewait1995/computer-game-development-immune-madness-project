// Heavily modified by Michelle Chu Rachel Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
//ammunition class

#include "REWammo2.h"

//=============================================================================
// default constructor
//=============================================================================
Ammunition2::Ammunition2() : Entity()
{
    spriteData.width = ammunition2::WIDTH;           // size of Ship1
    spriteData.height = ammunition2::HEIGHT;
    spriteData.x = ammunition2::X;                   // location on screen
    spriteData.y = ammunition2::Y;
    spriteData.rect.bottom = ammunition2::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = ammunition2::WIDTH;
    velocity.x = 500;                             // velocity X
    velocity.y = 0;                             // velocity Y
    radius = ammunition2::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;

	isLaunching = false;
	frameCounter = 0;
	curving = 0;
	isReset = true;
	curEnemy = NULL;
	location = D3DXVECTOR2(0,0);
}

Ammunition2::~Ammunition2()
{
	//delete the array with a for loop
	//SAFE_DELETE(curEnemyBac);
	ShowCursor(true);           // show cursor
}

void Ammunition2::initializeAmmo2(Graphics* g)
{
	ammoPart.initialize(g);
	return;
}
//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Ammunition2::update(float frameTime, PlasmaPlayer2 player2, int index, Audio* audio)
{
    Entity::update(frameTime);

	//Bacterium temp = &curEnemyBac;
	
	if(curEnemy != NULL)
	{
		setX(curEnemy->getX() + location.x);
		setY(curEnemy->getY() + location.y);
		setRadians(attachedRadians);
	}
	else
	{
		if(input->wasKeyPressed(VK_SPACE))
		{
			audio->playCue(THROW);
			isReset = false;
			isLaunching = true;
			frameCounter = 0;
			curving = 0;
		}

		if(isLaunching)
		{
			shoot(frameTime);
			
			velocity.x = AMMO_VELOCITY;

			// Bounce off walls
			// if hit bottom screen edge
			if (spriteData.y > GAME_HEIGHT-ammunition2::HEIGHT*getScale())
			{
				// position at bottom screen edge
				spriteData.y = GAME_HEIGHT-ammunition2::HEIGHT*getScale();
				velocity.y = -velocity.y;               // reverse Y direction
			}
			else if (spriteData.y < 0)                  // else if hit top screen edge
			{
				spriteData.y = 0;                       // position at top screen edge
				velocity.y = -velocity.y;               // reverse Y direction
			}
		}
		else
		{
			isReset = true;
			//tempCT.setAmmoBool1(true);
			setRadians((float)(3*PI/2));
			setX(player2.getX()-30);
			setY(player2.getY());
		}
	}

}


void Ammunition2::shoot(float frameTime)
{
	Entity::update(frameTime);
	VECTOR2 pos, vel;
	setX((float)(getX()-(velocity.x*frameTime)));
	setY((float)(getY()+velocity.y*frameTime + 0.001*curving));

	pos = getCenterPoint();
	vel = VECTOR2(5,5);
	ammoPart.createParticleEffect(pos,vel, 10);
	if(frameCounter > 400)
	{
		setRadians((float)(getRadians()-(PI/(2*NUM_FRAMES))));
		curving++;
	}
	else
	{
		curving--;
		setRadians((float)(getRadians()-(PI/(2*NUM_FRAMES))));
	}

	if(frameCounter == NUM_FRAMES/2)
		curving = 0;

	frameCounter++;

	if(frameCounter == NUM_FRAMES)
	{
		isLaunching = false;
		setRadians((float)(1.5*PI));
	}
	ammoPart.update(frameTime);
}

void Ammunition2::setVelocityX(int a)
{
	velocity.x *= a;
}

void Ammunition2::setVelocityY(int a)
{
	velocity.y *= a;
}

void Ammunition2::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void Ammunition2::setVisible()
{
	Image::setVisible(true);
	active = true;
}

bool Ammunition2::getReset()
{
	return isReset;
}

void Ammunition2::setLaunching()
{
	isLaunching = false;
}

void Ammunition2::setEnemy(Entity* curBacteria)
{
	curEnemy = curBacteria;
}

void Ammunition2::setLocation(VECTOR2 locationVec)
{
	location = locationVec;
	attachedRadians = getRadians();
	
	float halfY = (curEnemy->getHeight()*curEnemy->getScale())/2;
	float halfX = (curEnemy->getWidth()*curEnemy->getScale())/2;

	if ((location.y < halfY) && (location.x > halfX))
	{
		attachedRadians = getRadians() - PI;
		location.x -= 30;
	}
	else
	{
		attachedRadians = getRadians() - PI/2;
		location.y -= 30;
	}
}

void Ammunition2::drawAmmoPart()
{
	ammoPart.draw();
	return;
}
//void Ammunition2::deleteEatenAmmo(Entity* enemyEaten)
//{
//	
//}

// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#define WIN32_LEAN_AND_MEAN
#ifndef _REWAMMO2_H                 // Prevent multiple definitions if this 
#define _REWAMMO2_H                // file is included in more than one place
class Ammunition2;

#include "entity.h"
#include "constants.h"
#include "MLCbacterium.h"
//#include "collisionTypes.h"
#include "REWplayer2.h"
#include <vector>
#include "MLCAmmo2ParticleManger.h"
#include "audio.h"


namespace ammunition2
{
    const int WIDTH = 75;                   // image width
    const int HEIGHT = 75;                  // image height
    const int X = GAME_WIDTH/7 - WIDTH/7;   // location on screen
    const int Y = GAME_HEIGHT - HEIGHT;
    const float ROTATION_RATE = (float)PI/3; // radians per second
    const float SPEED = 250;                // 100 pixels per second
    const float MASS = 300.0f;              // mass
    const int   TEXTURE_COLS = 1;           // texture has 2 columns
}

// inherits from Entity class
class Ammunition2 : public Entity
{
private:
	bool isLaunching;
	int frameCounter;
	int curving;
	bool isReset;
	Entity* curEnemy;
	VECTOR2 location;
	float attachedRadians;
	Ammo2ParticleManager ammoPart;
public:
    // constructor
    Ammunition2();

	virtual ~Ammunition2();

    // inherited member functions
	void update(float frameTime, PlasmaPlayer2, int, Audio*);

	void shoot (float frameTime);
	void setVelocityX(int);
	void setVelocityY(int);
	void initializeAmmo2(Graphics* g);
	void setInvisible();

	void setVisible();
	bool getReset();
	void setLaunching();
	void setEnemy(Entity*);
	Entity* getEnemy() {return curEnemy;}
	void setLocation(VECTOR2);
	void drawAmmoPart();
	//void deleteEatenAmmo(Entity*);		//probs put this in collisions types

	//CollisionsTypes tempCT;
};
#endif


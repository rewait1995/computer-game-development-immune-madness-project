// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#define WIN32_LEAN_AND_MEAN
#ifndef _REWPLASMAPLAYER_H                 // Prevent multiple definitions if this 
#define _REWPLASMAPLAYER_H                // file is included in more than one place
class PlasmaPlayer;

#include "entity.h"
#include "constants.h"

namespace plasmaPlayer
{
    const int WIDTH = 172;                   // image width
    const int HEIGHT = 160;                  // image height
    const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    const float ROTATION_RATE = (float)PI/3; // radians per second
    const float SPEED = 250;                // 100 pixels per second
    const float MASS = 300.0f;              // mass
    const int   TEXTURE_COLS = 1;           // texture has 2 columns
}

// inherits from Entity class
class PlasmaPlayer : public Entity
{
public:
    // constructor
    PlasmaPlayer();

    // inherited member functions
    void update(float frameTime);
};
#endif


#ifndef _EATEN_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _EATEN_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"

class EatenParticleManager
{
	private:
	Particle particles[MAX_NUMBER_EATEN_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float EatenParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	EatenParticleManager();
	~EatenParticleManager();
	void EatenParticleManager::setInvisibleAllParticles();
	void EatenParticleManager::setVisibleNParticles(int n);
	void EatenParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void EatenParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool EatenParticleManager::initialize(Graphics *g);

	void EatenParticleManager::update(float frametime);
	void EatenParticleManager::draw();
	void EatenParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
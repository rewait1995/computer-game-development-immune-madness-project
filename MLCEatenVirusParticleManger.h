#ifndef _EATEN_VIRUS_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _EATEN_VIRUS_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"

class EatenVirusParticleManager
{
	private:
	Particle particles[MAX_NUMBER_EATEN_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float EatenVirusParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	EatenVirusParticleManager();
	~EatenVirusParticleManager();
	void EatenVirusParticleManager::setInvisibleAllParticles();
	void EatenVirusParticleManager::setVisibleNParticles(int n);
	void EatenVirusParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void EatenVirusParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool EatenVirusParticleManager::initialize(Graphics *g);

	void EatenVirusParticleManager::update(float frametime);
	void EatenVirusParticleManager::draw();
	void EatenVirusParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
#include "MLCEatenParticleManager.h"
#include <stdlib.h>
#include <time.h>
#include "graphics.h"

EatenParticleManager::EatenParticleManager()
{
	srand(time(NULL));
}
EatenParticleManager::~EatenParticleManager()
{
}
float EatenParticleManager::getVariance()
{
	float foo = (rand() );
	foo = ((int)foo	% 300)/100.0f;
	foo += 0.8f;
	return foo;
}
void EatenParticleManager::setInvisibleAllParticles()
{
	for (int i = 0; i < MAX_NUMBER_EATEN_PARTICLES; i++)
	{
		particles[i].setVisible(false);
		particles[i].setActive(false);
	}
	return;
}
void EatenParticleManager::setVisibleNParticles(int n)
{
	int activatedParticles = 0;
	for (int i = 0; i < MAX_NUMBER_EATEN_PARTICLES; i++)
	{
		if (!particles[i].getActive()) //found an inactive particle
		{
			particles[i].setActive(true);
			particles[i].setMaxTimeAlive(MAX_EATEN_PARTICLE_LIFETIME*getVariance());
			float newX = velocity.x * getVariance(); 
			float newY = velocity.y  * getVariance();
			VECTOR2 v = VECTOR2(newX,newY);
			particles[i].setX(position.x);
			particles[i].setY(position.y);
			particles[i].setVelocity(v);
			particles[i].setVisible(true);
			activatedParticles++;
			if (activatedParticles == n)
				return;
		}
	}
	return;
}

bool EatenParticleManager::initialize(Graphics *g)
{
	if (!tm.initialize(g, DNAPOLY_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dust texture"));
	for (int i = 0; i < MAX_NUMBER_EATEN_PARTICLES; i++)
	{
		if (!particles[i].initialize(g,0,0,0,&tm))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dust"));
		particles[i].setActive(false);
		particles[i].setVisible(false);
		particles[i].setScale(2.0f);
		particles[i].setRotationValue(0.6f);
	}
	return true;
}

void EatenParticleManager::update(float frametime)
{
	for (int i = 0; i < MAX_NUMBER_EATEN_PARTICLES; i++){
		if (particles[i].getActive())
			particles[i].update(frametime);

	}
	return;
}

void EatenParticleManager::draw()
{
	byte fadeAmount;
	COLOR_ARGB color;
	for (int i = 0; i < MAX_NUMBER_EATEN_PARTICLES; i++)
	{
		if (!particles[i].getActive())
			continue;
		float foo = particles[i].getMaxTimeAlive();  //MAX_PARTICLE_LIFETIME;
		float bar = particles[i].getTimeAlive();
		float foobar = (foo-bar)/foo;
		fadeAmount = 255 * foobar;
		color = D3DCOLOR_ARGB(fadeAmount,0xff,0xff,0xff);//fadeAmount,fadeAmount,fadeAmount);
		particles[i].draw(color);
		/*if (fadeAmount <= 10)
			particles[i].resetParticle();*/
	}
	return;
}

void EatenParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles)
{
	setPosition(pos);
	setVelocity(vel);
	setVisibleNParticles(numParticles);
	return;
}
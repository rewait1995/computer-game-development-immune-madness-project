#include "MLCvirus.h"

//=============================================================================
// default constructor
//=============================================================================
Virus::Virus() : Entity()
{
    spriteData.width = virusNS::WIDTH;           
    spriteData.height = virusNS::HEIGHT;
    spriteData.x = virusNS::X;                   // location on screen
    spriteData.y = virusNS::Y;
    spriteData.rect.bottom = virusNS::HEIGHT/2;    // rectangle to select parts of an image
    spriteData.rect.right = virusNS::WIDTH;
    
	velocity = D3DXVECTOR2(1,1);
    startFrame = 0;              // first frame of ship animation
    endFrame     = 0;              // last frame of ship animation
    currentFrame = startFrame;
    radius = virusNS::WIDTH/2.0;                 // collision radius
    collision = false;
    collisionType =entityNS::BOX;// entityNS::CIRCLE;
    target = false;
	edge.bottom = -virusNS::HEIGHT/2;
	spriteData.scale = 1;
	active = true;
	setMass(5);
	speed = 50;
	antigen = 3;
}

bool Virus::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Virus::setInvisible()
{
	Image::setVisible(false);
	active = false;
	return;
}

void Virus::setVisible()
{
	Image::setVisible(true);
	active = true;
	return;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Virus::update(float frameTime)
{
	Entity::update(frameTime);

	if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}
	setX(getX() + velocity.x*frameTime);
	setY(getY() + velocity.y*frameTime);
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-virusNS::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-virusNS::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-virusNS::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-virusNS::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
	return;
}

bool Virus::isCovered()
{
	if((antigen - pink-blue) == 0)
		return true;
	else
		return false;
}

void Virus::incrementPink()
{
	pink++;
	return;
}

void Virus::incrementBlue()
{
	blue++;
	return;
}

int Virus::getNumCovered()
{
	return (pink+blue);
}
void Virus::clearVars()
{
	int pink = 0;
	int blue = 0;
	return;
}
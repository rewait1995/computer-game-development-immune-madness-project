#ifndef _EATEN_BACTERIA_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _EATEN_BACTERIA_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"

class EatenBacteriaParticleManager
{
	private:
	Particle particles[MAX_NUMBER_EATEN_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float EatenBacteriaParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	EatenBacteriaParticleManager();
	~EatenBacteriaParticleManager();
	void EatenBacteriaParticleManager::setInvisibleAllParticles();
	void EatenBacteriaParticleManager::setVisibleNParticles(int n);
	void EatenBacteriaParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void EatenBacteriaParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool EatenBacteriaParticleManager::initialize(Graphics *g);

	void EatenBacteriaParticleManager::update(float frametime);
	void EatenBacteriaParticleManager::draw();
	void EatenBacteriaParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
// Heavily modified by Michelle Chu Rachel Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
//ammunition class

#include "REWammo1.h"

//=============================================================================
// default constructor
//=============================================================================
Ammunition1::Ammunition1() : Entity()
{
	spriteData.width = ammunition1::WIDTH;           // size of Ship1
	spriteData.height = ammunition1::HEIGHT;
	spriteData.x = ammunition1::X;                   // location on screen
	spriteData.y = ammunition1::Y;
	spriteData.rect.bottom = ammunition1::HEIGHT;    // rectangle to select parts of an image
	spriteData.rect.right = ammunition1::WIDTH;
	velocity.x = AMMO_VELOCITY;                             // velocity X
	velocity.y = 0;                             // velocity Y
	radius = ammunition1::WIDTH/2.0;
	collisionType = entityNS::CIRCLE;

	isLaunching = false;
	frameCounter = 0;
	curving = 0;
	isFirstFrame = true;
	isReset = true;
	int enemyFollow = -1;
	curEnemy = NULL;
	location = D3DXVECTOR2(0,0);
	//player2ptr = player2;
}

Ammunition1::~Ammunition1()
{
	//SAFE_DELETE(curEnemyBac);
	ShowCursor(true);           // show cursor
}

void Ammunition1::initializeAmmo1(Graphics* g)
{
	//ammoPart.initialize(g);
	return;
}
//void Ammunition1::initialize(Graphics* graphics, Game* game, int a)
//{
//
//}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Ammunition1::update(float frameTime, PlasmaPlayer1 player1, int index, Audio* audio)
{
	Entity::update(frameTime);
	//ammoPart.update(frameTime);
	//Bacterium temp = &curEnemyBac;

	if(curEnemy != NULL)
	{
		//setX(curEnemy->getX()+curEnemy->getWidth()*curEnemy->getScale()-(50*index));
		//setY(curEnemy->getY()-(10*index));
		setX(curEnemy->getX() + location.x);
		setY(curEnemy->getY() + location.y);


		setRadians(attachedRadians);
	}
	else
	{
		if(input->wasKeyPressed(VK_SHIFT))
		{
			audio->playCue(THROW);
			isReset = false;
			isLaunching = true;
			frameCounter = 0;
			curving = 0;
		}

		if(isLaunching)
		{
			shoot(frameTime);
			velocity.x = AMMO_VELOCITY;
			// Bounce off walls
			// if hit bottom screen edge
			if (spriteData.y > GAME_HEIGHT-ammunition1::HEIGHT*getScale())
			{
				// position at bottom screen edge
				spriteData.y = GAME_HEIGHT-ammunition1::HEIGHT*getScale();
				velocity.y = -velocity.y;               // reverse Y direction
			}
			else if (spriteData.y < 0)                  // else if hit top screen edge
			{
				spriteData.y = 0;                       // position at top screen edge
				velocity.y = -velocity.y;               // reverse Y direction
			}
		}
		else
		{
			isReset = true;
			//tempCT.setAmmoBool1(true);
			setRadians(0);
			setX(player1.getX()+player1.getWidth()*player1.getScale()-45);
			setY(player1.getY());
		}
	}
	return;
}

void Ammunition1::shoot(float frameTime)
{
	Entity::update(frameTime);
	

	setX((float)(getX()+velocity.x*frameTime));
	setY((float)(getY()+velocity.y*frameTime + 0.001*curving));
	
	if(frameCounter > 20)
	{
		setRadians(getRadians()+((float)(PI/(2*NUM_FRAMES))));
		curving++;
	}
	else
	{
		curving--;
		setRadians(getRadians()+((float)(PI/(2*NUM_FRAMES))));
	}

	if(frameCounter == NUM_FRAMES/2)
		curving = 0;

	frameCounter++;

	if(frameCounter == NUM_FRAMES)
	{
		isLaunching = false;
		setRadians(0);
	}
	/*VECTOR2 pos, vel;
	pos = getCenterPoint();
	vel = VECTOR2(-1,1);
	ammoPart.createParticleEffect(pos,vel, 10);*/
	return;
}

void Ammunition1::setInvisible()
{
	Image::setVisible(false);
	active = false;
	return;
}

void Ammunition1::setVisible()
{
	Image::setVisible(true);
	active = true;
	return;
}

void Ammunition1::setVelocityX(int a)
{
	velocity.x *= a;
	return;
}

void Ammunition1::setVelocityY(int a)
{
	velocity.y *= a;
	return;
}

bool Ammunition1::getReset()
{
	return isReset;
}

void Ammunition1::setLaunching()
{
	isLaunching = false;
	return;
}

void Ammunition1::setEnemy(Entity* curBacteria)
{
	curEnemy = curBacteria;
	return;
}

void Ammunition1::setLocation(VECTOR2 locationVec)
{
	location = locationVec;
	
	float halfY = (curEnemy->getHeight()*curEnemy->getScale())/2;
	float halfX = (curEnemy->getWidth()*curEnemy->getScale())/2;

	if ((location.y < halfY) && (location.x < halfX))
	{
		attachedRadians = getRadians() + PI/2;
		location.x += 30;
	}
	else
	{
		attachedRadians = getRadians();
		location.y -= 10;
		location.x += 20;
	}
	return;
}
//void Ammunition1::drawAmmoPart()
//{
//	ammoPart.draw();
//	return;
//}
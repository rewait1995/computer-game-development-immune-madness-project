#define WIN32_LEAN_AND_MEAN
#ifndef MLC_VIRUS_MANAGER_H
#define MLC_VIRUS_MANAGER_H

#include "MLCvirus.h"
#include <vector>
//#include "MLCgameVars.h"
#include "MLCVirusParticleManger.h"

class VirusManager : Virus //, GameVars
{
private:
	TextureManager virusTM;
	Entity targetEntity;
	int virusesleft;
	int numVirus;
	VirusParticleManager virusPart;

public:
	VirusManager();
	~VirusManager();
	void initialize(Graphics* graphics, Game* game, int a);
	void update(float frameTime);
	void render(Graphics* graphics);
	void ai(float time, Entity &t, int sightDistance);
	void evade(int sightDistance);
	int getVirusesLeft();
	int getNumVirus() {return numVirus;}
	int collision(Entity t, int a);
	
	int collisionWAmmo(Entity t);
	void decrementVirusesLeft();
	void reset();
	std::vector<Virus*> viruses;
};
#endif
#define WIN32_LEAN_AND_MEAN
#ifndef MLC_GAME_VARS
#define MLC_GAME_VARS

class GameVars
{
private:
	int numBacteria;
	int numVirus;
	int level;
public:
	GameVars();
	GameVars(int b, int v);
	int getNumBact();
	void setNumBact(int a);
	int getNumVirus();
	void setNumVirus(int a);
	int getLevel() {return level;}
};
#endif
#include "MLCvirusManager.h"

VirusManager::VirusManager()
{
}
VirusManager::~VirusManager()
{
	virusTM.onLostDevice();
	virusTM.onResetDevice();
	for(int i = 0; i < getNumVirus(); i++)
	{
		SAFE_DELETE(viruses[i]);
	}
}
void VirusManager::initialize(Graphics* graphics, Game* game, int a)
{
	numVirus = a;
	virusesleft = numVirus;
	for(int i = 0; i < numVirus; i++)
	{
		viruses.push_back(nullptr);
	}
	//initiating array of unactivated virus pointers
	for(int i = 0; i < numVirus; i++)
	{
		viruses[i] = new Virus;
	}

	if (!virusTM.initialize(graphics, VIRUS_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing virus texture"));

	for(int i = 0; i < numVirus; i++)
	{
		if(!viruses[i]->initialize(game, virusNS::WIDTH, virusNS::HEIGHT, virusNS::TEXTURE_COLS, &virusTM))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing virus texture"));
		viruses[i]->setX((float)GAME_WIDTH/(rand()%10+1));
		viruses[i]->setY((float)GAME_HEIGHT/(rand()%10+1));
		viruses[i]->setVisible();
		viruses[i]->setVelocity(VECTOR2((rand()%5+1)*virusNS::SPEED_X, (rand()%5+1)*-virusNS::SPEED_Y));
		viruses[i]->setCollisionType(entityNS::CIRCLE);
		viruses[i]->setEdge(COLLISION_BOX_VIRUS);
		//viruses[i]->setCollisionRadius(COLLISION_RADIUS_BACTERIA);
		viruses[i]->setScale(1);
		viruses[i]->setFrames(VIRUS_WIGGLE_START, VIRUS_WIGGLE_END);   // animation frames
		viruses[i]->setCurrentFrame(VIRUS_WIGGLE_START);     // starting frame
		viruses[i]->setFrameDelay(VIRUS_ANIMATION_DELAY);
	}
	virusPart.initialize(graphics);
	return;
}
void VirusManager::update(float frameTime)
{
	VECTOR2 pos, vel;
	for(int i = 0; i < numVirus; i++)
	{
		if(viruses[i]->getVisible())
		{
			viruses[i]->update(frameTime);
			pos = viruses[i]->getCenterPoint();
			vel = VECTOR2(.5, .5);
			virusPart.createParticleEffect(pos, vel, 1);
			virusPart.update(frameTime);
		}
	}
	return;
}
void VirusManager::render(Graphics* graphics)
{
	for(int i = 0; i <	numVirus; i++)
	{
		if(viruses[i]->getVisible())
			virusPart.draw();
	}
	graphics->spriteBegin();                // begin drawing sprites
	for(int i = 0; i <	numVirus; i++)
	{
		viruses[i]->draw();
	}
	
    graphics->spriteEnd();    
	return;
}
void VirusManager::ai(float time, Entity &t, int sightDistance)
{
	targetEntity = t;
	evade(sightDistance);
	return;
}
void VirusManager::evade(int sightDistance)
{
	for(int i = 0; i < numVirus; i++)
	{
		VECTOR2 vel = viruses[i]->getCenterPoint() - targetEntity.getCenterPoint();
		float length = D3DXVec2Length(&vel);
		if (length < sightDistance)
		{
			viruses[i]->setVelocity(vel);
			return;
		}
	}
	return;
}

int VirusManager::getVirusesLeft()
{
	return virusesleft;
}

int VirusManager::collision(Entity t, int a)
{
	VECTOR2 collisionVector;
	// if collision between killer cell and virus
	for(int i = 0; i < numVirus; i++)
	{
		//bounce off killer cell
		if(viruses[i]->collidesWith(t, collisionVector) && (a == -1))
			viruses[i]->bounce(collisionVector, t);
		else if(viruses[i]->collidesWith(t, collisionVector) && (a == 1))
		{
			viruses[i]->bounce(collisionVector, t);
			t.bounce(collisionVector, *viruses[i]);
		}
		for(int j = 0; j < getNumVirus(); j++)
		{
			if(viruses[i]->collidesWith(*viruses[j], collisionVector))
			{
				//viruses bounce off each other
				viruses[i]->bounce(collisionVector, *viruses[j]);
				viruses[j]->bounce(collisionVector*-1, *viruses[i]);
			}
		}
	}
	return 0;
}

int VirusManager::collisionWAmmo(Entity t)
{
	VECTOR2 collisionVector;
	for(int i = 0; i < numVirus; i++)
	{
		if(viruses[i]->collidesWith(t, collisionVector))
		{
			return i;
		}
	}
	return -1;
}
void VirusManager::decrementVirusesLeft()
{
	virusesleft--;
	return;
}
void VirusManager::reset()
{
	virusesleft = numVirus;
	for(int i = 0; i < getNumVirus(); i++)
	{
		viruses[i]->setVisible();
	}
	return;
}
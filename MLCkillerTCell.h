#define WIN32_LEAN_AND_MEAN
#ifndef MLC_KILLER_T_CELL
#define MLC_KILLER_T_CELL

#include "entity.h"
#include "constants.h"

namespace killerTCellNS
{
	const int WIDTH = 120;                   // image width
    const int HEIGHT = 120;                  // image height
    const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    const float SPEED_X = 100;
	const float SPEED_Y = 100;
	const int   TEXTURE_COLS = 4;           // texture has 2 columns
}

//inherit from entity
class killerTCell:public Entity
{
private:
   // puckNS::DIRECTION direction;    
    bool collision;                 
    bool target;  
	int directionX;
	int directionY;
	VECTOR2 velocity;
	float speed;
	//Entity targetEntity;
	//Game* game;

public:
    // constructor
    killerTCell();
    // inherited member functions
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);

    // Set collision Boolean
    void setCollision(bool c)
    {collision = c;}

    // Set collision type (NONE, CIRCLE, BOX, ROTATED_BOX)
    virtual void setCollisionType(entityNS::COLLISION_TYPE ctype)
    {collisionType = ctype;}

    // Set RECT structure used for BOX and ROTATED_BOX collision detection.
    void setEdge(RECT e) {edge = e;}

    // Set target
    void setTarget(bool t) {target = t;}

    // Get collision
    bool getCollision() {return collision;}

    // Get collision type
    entityNS::COLLISION_TYPE getCollisionType() {return collisionType;}

	void setInvisible();

	void setVisible();

	void setVelocity(VECTOR2 v) {velocity = v;}

	VECTOR2 getVelocity() {return velocity;}

	float getSpeed() {return speed;}

};
#endif
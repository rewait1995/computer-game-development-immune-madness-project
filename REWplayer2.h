// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#define WIN32_LEAN_AND_MEAN
#ifndef _REWPLAYER2_H                 // Prevent multiple definitions if this 
#define _REWPLAYER2_H                // file is included in more than one place
class PlasmaPlayer2;

#include "entity.h"
#include "constants.h"

namespace plasmaPlayer2
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 128;                  // image height
    const int X = GAME_WIDTH/7 - WIDTH/7;   // location on screen
    const int Y = GAME_HEIGHT - HEIGHT;
    const float ROTATION_RATE = (float)PI/3; // radians per second
    const float SPEED = 250;                // 100 pixels per second
    const float MASS = 300.0f;              // mass
    const int   TEXTURE_COLS = 1;           // texture has 2 columns
}

// inherits from Entity class
class PlasmaPlayer2 : public Entity
{
private:
	bool eaten;
	int score;
public:
    // constructor
    PlasmaPlayer2();

    // inherited member functions
    void update(float frameTime);
	bool isEaten() {return eaten;}
	void setEaten(bool a);
	int getScore() {return score;}
	void setScore(int a);
	void setInvisible();

	void setVisible();
};
#endif


#include "MLCkillerTCell.h"

killerTCell::killerTCell() : Entity()
{
    spriteData.width = killerTCellNS::WIDTH;           
    spriteData.height = killerTCellNS::HEIGHT;
    spriteData.x = killerTCellNS::X;                   // location on screen
    spriteData.y = killerTCellNS::Y;
    //spriteData.rect.bottom = killerTCellNS::HEIGHT;    // rectangle to select parts of an image
    //spriteData.rect.right = killerTCellNS::WIDTH;
    
	velocity = D3DXVECTOR2(1,1);
    startFrame = 0;              // first frame of ship animation
    endFrame     = 0;              // last frame of ship animation
    currentFrame = startFrame;
	radius = ((killerTCellNS::WIDTH/2)*getScale());                 // collision radius
    collision = false;
	collisionType = entityNS::CIRCLE;// entityNS::CIRCLE;
    target = false;
	//edge.bottom = -killerTCellNS::HEIGHT/2;
	spriteData.scale = 1;
	active = true;
	speed = 100;
	setMass(500);
}

bool killerTCell::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void killerTCell::setInvisible()
{
	Image::setVisible(false);
	active = false;
	return;
}

void killerTCell::setVisible()
{
	Image::setVisible(true);
	active = true;
	return;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void killerTCell::update(float frameTime)
{
	 Entity::update(frameTime);
	VECTOR2 foo = velocity*frameTime*speed;
	if (getPositionX() + Image::getWidth()*Image::getScale() > GAME_WIDTH)
	{
		setPosition(D3DXVECTOR2(0,getPositionY()));
	}
	if (getPositionX() < 0)
	{
		setPosition(D3DXVECTOR2(GAME_WIDTH-Image::getWidth()*Image::getScale(),getPositionY()));
	}
	if (getPositionY() + Image::getHeight()*Image::getScale() > GAME_HEIGHT)
	{
		setPosition(D3DXVECTOR2(getPositionX(),0));
	}
	if (getPositionY() < 0)
	{
		setPosition(D3DXVECTOR2(getPositionX(),GAME_WIDTH-Image::getHeight()*Image::getScale()));
	}

	velocity = D3DXVECTOR2(0,0);
	incPosition(foo);
	Image::setX(getPositionX());
	Image::setY(getPositionY());
	return;
}
#ifndef _AMMO1_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _AMMO1_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"

class Ammo1ParticleManager
{
	private:
	Particle particles[MAX_NUMBER_AMMO_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float Ammo1ParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	Ammo1ParticleManager();
	~Ammo1ParticleManager();
	void Ammo1ParticleManager::setInvisibleAllParticles();
	void Ammo1ParticleManager::setVisibleNParticles(int n);
	void Ammo1ParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void Ammo1ParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool Ammo1ParticleManager::initialize(Graphics *g);

	void Ammo1ParticleManager::update(float frametime);
	void Ammo1ParticleManager::draw();
	void Ammo1ParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
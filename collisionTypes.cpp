// Programming 2D Games
// Copyright (c) 2011, 2012 by: 
// Charles Kelly
// Collision types demo
// Press '1', '2' or '3' to select collision type for ship.

#include "collisionTypes.h"
//blah
//=============================================================================
// Constructor
//=============================================================================
CollisionTypes::CollisionTypes()
{
	timeInState = 0;
	gamestate = splash;
	//lastGameState = splash;
	level1Initialize = false;
	level2Initialize = false;
	ammoBool1 = true;
	ammoBool2 = true;
	leaveMenu = true;
	resetLevel1Pics = false;
	resetLevel1Params = false;
	resetLevel2Pics = false;
	resetLevel2Params = false;
	checkPointThrough = false;
	sound = true;
	invincible = false;
	chaseBactOnly = false;
	level2MusicOn = false;
	/*eatenBacteria = 0;
	eatenVirus = 0;*/
	playerEaten = 0;
	eatenBacteria.resize(0);
	eatenVirus.resize(0);

	//minutes = 3;
	//seconds = 0;

	locationVec = D3DXVECTOR2(1,1);
	//redScore = 0;
	//blueScore = 0;
}

//=============================================================================
// Destructor
//=============================================================================
CollisionTypes::~CollisionTypes()
{
	deleteAll();                // free all reserved memory
	ShowCursor(true);           // show cursor
}

void CollisionTypes::deleteAll()
{
	releaseAll();               // call onLostDevice() for every graphics item
	SAFE_DELETE(output);
	for(unsigned int i = 0; i < blueAmmo.size(); i++)
	{
		SAFE_DELETE(blueAmmo[i]);
	}
	for(unsigned int i = 0; i < redAmmo.size(); i++)
	{
		SAFE_DELETE(redAmmo[i]);
	}
	initialized = false;
	return;
}

void CollisionTypes::gameStateUpdate()
{
	//MLC setting up game states
	timeInState += frameTime;
	if((gamestate == splash) && (timeInState > 2))
	{
		//audio->stopCue(LEVEL2);
		audio->playCue(BACKGROUND);
		gamestate = menu;
		timeInState = 0;
	}
	else if((gamestate == menu) && (input->wasKeyPressed(VK_RETURN)) && (gameMenu.linePtr == 0))
	{
		if(sound)
		{
			audio->playCue(SELECT);
		}
		if(resetLevel1Params == true)
		{
			player2.setPosition(VECTOR2(0,0));
			player1.setPosition(VECTOR2(0, 130));
			player1.setEaten(false);
			player2.setEaten(false);
			player1.setScore(0);
			player2.setScore(0);
			killerCell.reset();
			killerCell2.reset();
			bacteria1.reset();
			resetAmmo();
			playerEaten = 0;
			resetLevel1Params = false;
		}
		if(resetLevel1Pics == true)
		{
			player1.setVisible();
			player2.setVisible();
			player1Ammo.setVisible();
			player2Ammo.setVisible();
			blueAmmo.resize(0);
			redAmmo.resize(0);
			seconds = 0;
			minutes = 4;
			resetLevel1Pics = false;
		}


		gamestate = level1;
		timeInState = 0;
	}
	else if((gamestate == menu) && (input->wasKeyPressed(VK_RETURN)) && (gameMenu.linePtr == 1))
	{
		if(sound)
			audio->playCue(SELECT);
		gamestate = highscore;
		timeInState = 0;
	}
	else if((gamestate == menu) && (input->wasKeyPressed(VK_RETURN)) && (gameMenu.linePtr == 2))
	{
		if(sound)
			audio->playCue(SELECT);
		gamestate = setting;
		gameMenu.subLinePtr = 0;
		gameMenu.subMenuShow = true;
		gameMenu.menuShow = false;
		timeInState = 0;
	}
	else if((gamestate == menu) && (input->wasKeyPressed(VK_RETURN)) && (gameMenu.linePtr == 3))
	{
		if(sound)
			audio->playCue(SELECT);
		gamestate = cat;
		timeInState = 0;
	}

	else if ((gamestate == highscore) && (input->wasKeyPressed(VK_BACK)))
	{
		if(sound)
			audio->playCue(BACK);
		gamestate = menu;
		timeInState = 0;
	}
	else if ((gamestate == setting) && (input->wasKeyPressed(VK_BACK)))
	{
		if(sound)
			audio->playCue(BACK);
		gamestate = menu;
		gameMenu.subMenuShow = false;
		gameMenu.menuShow = true;
		timeInState = 0;
	}
	else if ((gamestate == cat) && (input->wasKeyPressed(VK_BACK)))
	{
		if(sound)
			audio->playCue(BACK);
		gamestate = menu;
		timeInState = 0;
	}
	else if((gamestate == level1Clear)&&(timeInState > 2))
	{
		gamestate = level2;
		timeInState = 0;
	}
	else if ((gamestate == level1) && (bacteria1.getBacteriaLeft() == 0))
	{
		if(sound)
			audio->playCue(WIN);
		gamestate = level1Clear;
		player1ScoreCHK = player1.getScore();
		player2ScoreCHK = player2.getScore();
		timeInState = 0;
	}
	else if ((gamestate == level1) && (playerEaten == 2))
	{
		if(sound)
			audio->playCue(LOSE);
		gamestate = lose;
		timeInState = 0;
	}
	else if ((gamestate == level2) && (bacteria2.getBacteriaLeft() == 0) && (viruses2.getVirusesLeft() == 0))
	{
		if(sound)
		{
			if(!invincible)
			{
				audio->stopCue(LEVEL2);
				audio->playCue(BACKGROUND);
			}
			audio->playCue(WIN);
		}
		gamestate = win;
		timeInState = 0;
	}
	else if((gamestate == level2)&& (playerEaten == 2) && (checkPointThrough == false))
	{
		gamestate = level2CheckPoint;
		resetLevel2Pics = true;
		resetLevel2Params = true;
		checkPointThrough = true;
		timeInState = 0;
	}
	else if((gamestate == level2) && (playerEaten == 2) && (checkPointThrough == true))
	{
		if(sound)
		{
			if(!invincible)
			{
				audio->stopCue(LEVEL2);
				audio->playCue(BACKGROUND);
			}
			audio->playCue(LOSE);
		}
		gamestate = lose;
		timeInState = 0;
	}
	else if((gamestate == level2CheckPoint) && (input->wasKeyPressed(VK_RETURN)))
	{
		if(sound)
			audio->playCue(SELECT);
		if(resetLevel2Params == true)
		{
			killerCell.reset();
			killerCell2.reset();
			player2.setPosition(VECTOR2(0,0));
			player1.setPosition(VECTOR2(0, 130));
			player2.setEaten(false);
			player1.setEaten(false);
			playerEaten = 0;
			bacteria2.reset();
			viruses2.reset();
			resetAmmo();
			resetLevel2Params = false;
		}
		if(resetLevel2Pics == true)
		{
			blueAmmo.resize(0);
			redAmmo.resize(0);
			player2.setVisible();
			player1Ammo.setVisible();
			player1.setVisible();
			player2Ammo.setVisible();

			resetLevel1Pics = false;
		}
		player1.setScore(player1ScoreCHK);
		player2.setScore(player2ScoreCHK);
		gamestate = level2;
		timeInState = 0;
	}
	else if((gamestate == lose) && (input->wasKeyPressed(VK_RETURN)))
	{
		if(sound)
			audio->playCue(SELECT);
		gamestate = menu;
		timeInState = 0;
	}
	else if((gamestate == win) && (input->wasKeyPressed(VK_RETURN)))
	{
		if(sound)
			audio->playCue(SELECT);
		gamestate = menu;
		timeInState = 0;
	}
	return;
}
//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void CollisionTypes::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	//REW setting up and initializing the textures for win screens and splash screen
	if (!winTM.initialize(graphics,WIN_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!winPic.initialize(graphics, winTM.getWidth(), winTM.getHeight(), 0,&winTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	if (!loseTM.initialize(graphics,LOSE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!losePic.initialize(graphics, loseTM.getWidth(), loseTM.getHeight(), 0,&loseTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));

	killerCell.initializeTCellandTM(graphics, this);
	killerCell.setAnimationInitial();
	killerCell2.initializeTCellandTM(graphics, this);
	killerCell2.setAnimationInitial();
	//REW setting up and initializing the textures for players and ammo
	//PLAYER 1 IS BLUE
	if (!player1TM.initialize(graphics,PLAYER1_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!player1.initialize(this, P1_WIDTH, P1_HEIGHT, P1_COLS,&player1TM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	//player1.setPosition(VECTOR2((float)0.75*GAME_WIDTH, (float)0.75*(GAME_HEIGHT-2*paddle.getHeight())));
	player1.setPosition(VECTOR2(0,0));
	player1.setCollisionType(entityNS::BOX);
	player1.setEdge(COLLISION_BOX_PADDLE);
	player1.setCollisionRadius(COLLISION_RADIUS);
	player1.setScale(1);
	player1.setFrames(P1_ANIMATE_START, P1_ANIMATE_END);   // animation frames
	player1.setCurrentFrame(P1_ANIMATE_START);     // starting frame
	player1.setFrameDelay(P1_ANIMATION_DELAY);

	//PLAYER 2 IS RED
	if (!player2TM.initialize(graphics,PLAYER2_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!player2.initialize(this, P2_WIDTH, P2_HEIGHT, P2_COLS,&player2TM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	//player2.setPosition(VECTOR2((float)0.75*GAME_WIDTH, (float)0.75*(GAME_HEIGHT-2*paddle.getHeight())));
	player2.setPosition(VECTOR2(0,130));
	player2.setCollisionType(entityNS::BOX);
	player2.setEdge(COLLISION_BOX_PADDLE);
	player2.setCollisionRadius(COLLISION_RADIUS);
	player2.setScale(1);
	player2.setFrames(P2_ANIMATE_START, P2_ANIMATE_END);   // animation frames
	player2.setCurrentFrame(P2_ANIMATE_START);     // starting frame
	player2.setFrameDelay(P2_ANIMATION_DELAY);

	if (!blueAmmoTM.initialize(graphics,PLAYER1_AMMO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!player1Ammo.initialize(this, blueAmmoTM.getWidth(), blueAmmoTM.getHeight(), 0,&blueAmmoTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	player1Ammo.setPosition(VECTOR2(player1.getX()+player1.getWidth()*player1.getScale(),player1.getY()));
	player1Ammo.setCollisionType(entityNS::BOX);
	player1Ammo.setEdge(COLLISION_BOX_PADDLE);
	player1Ammo.setCollisionRadius(COLLISION_RADIUS);
	player1Ammo.setScale(1);
	//player1Ammo.initializeAmmo1(graphics);

	if (!redAmmoTM.initialize(graphics,PLAYER2_AMMO_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!player2Ammo.initialize(this, redAmmoTM.getWidth(), redAmmoTM.getHeight(), 0,&redAmmoTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	player2Ammo.setPosition(VECTOR2(player2.getX()+player2.getWidth()*player2.getScale(),player2.getY()));
	player2Ammo.setCollisionType(entityNS::BOX);
	player2Ammo.setEdge(COLLISION_BOX_PADDLE);
	player2Ammo.setCollisionRadius(COLLISION_RADIUS);
	player2Ammo.setScale(1);
	player2Ammo.setRadians((float)(1.5*PI));
	//player2Ammo.initializeAmmo2(graphics);

	//MLC initializing and setting up animation for background, killer T Cell and foreign bodies
	//blebbing texture intialization
	if(!catTM.initialize(graphics, CAT_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing cat texture"));

	if(!catPic.initialize(graphics,GAME_WIDTH, GAME_HEIGHT, 1, &catTM))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing cat"));

	if (!background1TM.initialize(graphics,BACKGROUND1_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background texture"));
	if (!background1.initialize(graphics,GAME_WIDTH, GAME_HEIGHT, 1, &background1TM))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background"));

	if (!background2TM.initialize(graphics,BACKGROUND2_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background texture"));
	if (!background2.initialize(graphics,GAME_WIDTH, GAME_HEIGHT, 1, &background2TM))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing background"));

	if (!menuTM.initialize(graphics,MENU_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu texture"));

	//blebbing image
	if (!menuPic.initialize(graphics,GAME_WIDTH, GAME_HEIGHT, 1, &menuTM))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing Menu"));

	if (!splashTM.initialize(graphics,SPLASH_SCREEN))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!splashScreen.initialize(graphics, splashTM.getWidth(), splashTM.getHeight(), 0,&splashTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	if (!level1ClearTM.initialize(graphics,LEVEL1_DONE_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!level1ClearPic.initialize(graphics, GAME_WIDTH, GAME_HEIGHT, 1,&level1ClearTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
	if (!level2ChkPtTM.initialize(graphics,LEVEL2_CHKPOINT_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!level2ChkPtPic.initialize(graphics, GAME_WIDTH, GAME_HEIGHT, 1,&level2ChkPtTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));

	if (!highScoreTM.initialize(graphics,HIGHSCORES_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!highScorePic.initialize(graphics, GAME_WIDTH, GAME_HEIGHT, 1,&highScoreTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));

	if (!settingTM.initialize(graphics,SETTINGS_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing paddle texture"));
	if (!settingPic.initialize(graphics, GAME_WIDTH, GAME_HEIGHT, 1,&settingTM))
		throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));

	
	gameMenu.initialize(graphics, input);
	eatenParticles.initialize(graphics);
	ammo1Part.initialize(graphics);
	eatenPlayer1Part.initialize(graphics);
	eatenPlayer2Part.initialize(graphics);

	player1.setFrames(P1_ANIMATE_START, P1_ANIMATE_END);
	player2.setFrames(P2_ANIMATE_START, P2_ANIMATE_END);
	//REW initialize DirectX fonts
	output = new TextDX();
	if(output->initialize(graphics, 50, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing output font"));

	return;
}


//=============================================================================
// Update all game items
//=============================================================================
void CollisionTypes::update()
{
	VECTOR2 pos, vel;
	VECTOR2* normal;
	gameStateUpdate();
	switch(gamestate)
	{
	case splash:
		break;
	case menu:
		gameMenu.update(audio);
		break;
	case highscore:
		break;
	case setting:
		if((gameMenu.subLinePtr == 0) && (input->wasKeyPressed(VK_RETURN)) && (sound == true) && (timeInState > .2))
		{
			audio->playCue(SELECT);
			sound = false;
			if(invincible)
				audio->stopCue(INVINCIBLE);
			else
				audio->stopCue(BACKGROUND);
		}
		else if((gameMenu.subLinePtr == 0) && (input->wasKeyPressed(VK_RETURN)) && (sound == false))
		{
			audio->playCue(SELECT);
			sound = true;
			if(invincible)
				audio->playCue(INVINCIBLE);
			else
				audio->playCue(BACKGROUND);
		}
		if((gameMenu.subLinePtr == 1) && (input->wasKeyPressed(VK_RETURN)) && (invincible == true))
		{
			if(sound)
			{
				audio->playCue(SELECT);
				audio->stopCue(INVINCIBLE);
				audio->playCue(BACKGROUND);
			}
			invincible = false;
		}
		else if((gameMenu.subLinePtr == 1) && (input->wasKeyPressed(VK_RETURN)) && (invincible == false))
		{
			if(sound)
			{
				audio->playCue(SELECT);
				audio->stopCue(BACKGROUND);
				audio->playCue(INVINCIBLE);
			}
			invincible = true;
		}
		gameMenu.update(audio);
		eatenParticles.update(frameTime);
		break;
	case cat:
		break;
	case level1:
		if(input->isKeyDown(VK_BACK))
			gamestate = menu;
		if(level1Initialize == false)
		{
			bacteria1.initialize(graphics, this, 17);
			resetAmmo();
			level1Initialize = true;
		}
		
		for(unsigned int i = 0; i < blueAmmo.size(); i++)
		{
			blueAmmo[i]->update(frameTime, player1, i, audio);
		}
		for(unsigned int i = 0; i < redAmmo.size(); i++)
		{
			redAmmo[i]->update(frameTime, player2, i, audio);
		}

		player1.update(frameTime);
		player2.update(frameTime);
		killerCell.update(frameTime);
		killerCell2.update(frameTime);
		bacteria1.update(frameTime);
		//eatenParticles.update(frameTime);
		player1Ammo.update(frameTime, player1, -1, audio);
		ammo1Part.update(frameTime);
		/*eatenPlayer1Part.update(frameTime);
		eatenPlayer2Part.update(frameTime);*/
		//ammo1Part.createParticleEffect(VECTOR2(player1Ammo.getCenterPoint()), VECTOR2(-player1Ammo.getVelocity()), 20);
		player2Ammo.update(frameTime, player2, -1, audio);

		
		pos = player1Ammo.getCenterPoint();
		vel = player1Ammo.getVelocity();
		//VECTOR2* normal = D3DXVec2Normalize(&vel, &vel);
		//ammo1Part.createParticleEffect(pos,vel, 5);
		player2Ammo.update(frameTime, player2, -1, audio);
		
		break;

	case level2:
		if(input->isKeyDown(VK_BACK))
			gamestate = menu;
		if(level2Initialize == false)
		{
			if(!invincible)
			{
				audio->stopCue(BACKGROUND);
				audio->playCue(LEVEL2);
			}
			viruses2.initialize(graphics, this, 12);
			bacteria2.initialize(graphics, this, 12);
			resetAmmo();
			level2Initialize = true;
		}
		player1.update(frameTime);
		player2.update(frameTime);
		killerCell.update(frameTime);
		killerCell2.update(frameTime);
		bacteria2.update(frameTime);
		viruses2.update(frameTime);
		//eatenPlayer1Part.update(frameTime);
		//eatenPlayer2Part.update(frameTime);

		for(unsigned int i = 0; i < blueAmmo.size(); i++)
		{
			blueAmmo[i]->update(frameTime, player1, i, audio);
		}
		for(unsigned int i = 0; i < redAmmo.size(); i++)
		{
			redAmmo[i]->update(frameTime, player2, i, audio);
		}
		player1Ammo.update(frameTime, player1, -1, audio);
		player2Ammo.update(frameTime, player2, -1, audio);
		break;
	case level1Clear:
		break;
	case level2CheckPoint:
		resetLevel2Params = true;
		resetLevel1Pics  = true;
		break;
	case win:
		gameMenu.update(audio);
		break;
	case lose:
		gameMenu.update(audio);
		resetLevel1Pics = true;
		resetLevel1Params = true;
		break;
	}

	if(input->isKeyDown(VK_ESCAPE))
		PostQuitMessage(0);
	return;
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void CollisionTypes::ai()
{
	//MLC Section
	switch(gamestate)
	{
	case level1:

		if((player1.getActive()) && (player2.getActive()))
		{
			killerCell.ai(frameTime, player1);
			killerCell2.ai(frameTime, player2);
		}
		if((player1.getActive()) && (!player2.getActive()))
		{
			killerCell.ai(frameTime, player1);
			killerCell2.ai(frameTime, player1);
		}
		if((!player1.getActive()) && (player2.getActive()))
		{
			killerCell.ai(frameTime, player2);
			killerCell2.ai(frameTime, player2);
		}
		bacteria1.ai(frameTime, player1, 200);
		bacteria1.ai(frameTime, player2, 200);

		break;

	case level2:


		if((player1.getActive()) && (player2.getActive()))
		{
			killerCell.ai(frameTime, player1);
			killerCell2.ai(frameTime, player2);
		}
		if((player1.getActive()) && (!player2.getActive()))
		{
			killerCell.ai(frameTime, player1);
			killerCell2.ai(frameTime, player1);
		}
		if((!player1.getActive()) && (player2.getActive()))
		{
			killerCell.ai(frameTime, player2);
			killerCell2.ai(frameTime, player2);
		}

		bacteria2.ai(frameTime, player1, 200);
		bacteria2.ai(frameTime, player2, 200);
		viruses2.ai(frameTime, player1, 200);
		viruses2.ai(frameTime, player2, 200);
		break;
	}
	return;
}

//=============================================================================
// Handle collisions
//=============================================================================
void CollisionTypes::collisions()
{
	VECTOR2 pos, vel;
	switch(gamestate)
	{
	case level1:

		bacteria1.collision(killerCell.getKillerCell(), -1);
		bacteria1.collision(player1, 1);
		bacteria1.collision(player2, 1);

		if(!invincible)
		{
			if(killerCell.collision(player1))
			{
				if(sound == true)
					audio->playCue(EAT);
				player1.setInvisible();
				player1Ammo.setInvisible();
				player1.setEaten(true);
				playerEaten++;
			}
			if(killerCell.collision(player2))
			{
				if(sound == true)
					audio->playCue(EAT);
				player2.setInvisible();
				player2Ammo.setInvisible();
				player2.setEaten(true);
				playerEaten++;
			}
			if(killerCell2.collision(player1))
			{
				if(sound == true)
					audio->playCue(EAT);
				player1.setInvisible();
				player1Ammo.setInvisible();
				player1.setEaten(true);
				playerEaten++;
			}
			if(killerCell2.collision(player2))
			{
				if(sound == true)
					audio->playCue(EAT);
				player2.setInvisible();
				player2Ammo.setInvisible();
				player2.setEaten(true);
				playerEaten++;
			}
		}
		if(player1.collidesWith(player2, collisionVector))
			player1.bounce(collisionVector, player2);
		if(player2.collidesWith(player1, collisionVector))
			player2.bounce(collisionVector, player1);

		if(player1Ammo.collidesWith(player2Ammo,collisionVector))
		{
			//player1Ammo.bounce(collisionVector,player2Ammo);
			player1Ammo.setVelocityX(-1);
			//player1Ammo.setRadians(PI/2);
			player2Ammo.setVelocityX(1);
			player1Ammo.setVelocityY(-50);
			player2Ammo.setVelocityY(1);
		}

		//ammo collides with bacteria
		for(int i = 0; i < bacteria1.getNumBact(); i++)
		{
			int curBacteria = bacteria1.collisionWAmmo(player1Ammo);
			if((curBacteria != -1) && ammoBool2 && !player1Ammo.getReset())
			{
				player2.setScore(player2.getScore()+100);
				bacteria1.bacteria[curBacteria]->setInvisible();
				bacteria1.decrementBacteriaLeft();
				ammoBool2 = false;
				if(sound == true)
					audio->playCue(HIT);
				
				blueAmmo.push_back(nullptr);
				blueAmmo[blueAmmo.size()-1] = new Ammunition1;


				//float curX = player1Ammo.getX();
				//float curY = player1Ammo.getY();
				

				if(blueAmmo.size() > 0)		//function where pass in the index in the array and set up the texture
				{
					if (!blueAmmo[blueAmmo.size()-1]->initialize(this, blueAmmoTM.getWidth(), blueAmmoTM.getHeight(), 0,&blueAmmoTM))
						throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
					blueAmmo[blueAmmo.size()-1]->setPosition(VECTOR2(player1.getX()+player1.getWidth()*player1.getScale(),player1.getY()));
					blueAmmo[blueAmmo.size()-1]->setCollisionType(entityNS::BOX);
					blueAmmo[blueAmmo.size()-1]->setEdge(COLLISION_BOX_PADDLE);
					blueAmmo[blueAmmo.size()-1]->setCollisionRadius(COLLISION_RADIUS);
					blueAmmo[blueAmmo.size()-1]->setScale(1);
					blueAmmo[blueAmmo.size()-1]->initializeAmmo1(graphics);
				}


				player1Ammo.setLaunching();

			}
			//if(bacteriaM.collisionWAmmo(player2Ammo) && ammoBool1)

			curBacteria = bacteria1.collisionWAmmo(player2Ammo);
			if((curBacteria != -1) && ammoBool1 && !player2Ammo.getReset())
				//if(bacteriaM.collisionWAmmo(player1Ammo))
			{
				float curX = player2Ammo.getX();
				float curY = player2Ammo.getY();

				ammoBool1 = false;
				if(sound == true)
					audio->playCue(HIT);
				bacteria1.bacteria[curBacteria]->setInvisible();
				bacteria1.decrementBacteriaLeft();
				player1.setScore(player1.getScore()+100);

				redAmmo.push_back(nullptr);
				redAmmo[redAmmo.size()-1] = new Ammunition2;
			
				//audio->playCue(HIT);

				if(redAmmo.size() > 0)		//function where pass in the index in the array and set up the texture
				{
					if (!redAmmo[redAmmo.size()-1]->initialize(this, redAmmoTM.getWidth(), redAmmoTM.getHeight(), 0,&redAmmoTM))
						throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
					redAmmo[redAmmo.size()-1]->setPosition(VECTOR2(player2.getX()-30,player2.getY()));
					redAmmo[redAmmo.size()-1]->setCollisionType(entityNS::BOX);
					redAmmo[redAmmo.size()-1]->setEdge(COLLISION_BOX_PADDLE);
					redAmmo[redAmmo.size()-1]->setCollisionRadius(COLLISION_RADIUS);
					redAmmo[redAmmo.size()-1]->setScale(1);
					redAmmo[redAmmo.size()-1]->initializeAmmo2(graphics);

					player2Ammo.setLaunching();
				}

			}

			if(player1Ammo.getReset())
			{
				ammoBool2 = true;
			}

			if(player2Ammo.getReset())
			{
				ammoBool1 = true;
			}
		}
		break;
	case level2:

		bacteria2.collision(killerCell.getKillerCell(), -1);
		viruses2.collision(killerCell.getKillerCell(), -1);
		bacteria2.collision(player1, 1);
		bacteria2.collision(player2, 1);
		viruses2.collision(player1, 1);
		viruses2.collision(player2, 1);


		if(!invincible)
		{
			if(killerCell.collision(player1) && (player1.getVisible() == true))
			{
				if(sound == true)
					audio->playCue(EAT);
				player1.setInvisible();
				player1Ammo.setInvisible();
				player1.setEaten(true);
				playerEaten++;
			}
			if(killerCell.collision(player2) && (player2.getVisible()==true))
			{
				if(sound == true)
					audio->playCue(EAT);
				player2.setInvisible();
				player2Ammo.setInvisible();
				player2.setEaten(true);
				playerEaten++;
			}
			if(killerCell2.collision(player1) && (player1.getVisible()))
			{
				if(sound == true)
					audio->playCue(EAT);
				player1.setInvisible();
				player1Ammo.setInvisible();
				player1.setEaten(true);
				playerEaten++;
			}
			if(killerCell2.collision(player2) && (player2.getVisible()))
			{
				if(sound == true)
					audio->playCue(EAT);
				player2.setInvisible();
				killerCell.firstPlayerEaten == 1;
				player2Ammo.setInvisible();
				player2.setEaten(true);
				playerEaten++;
			}
		}

		if(player1.collidesWith(player2, collisionVector))
			player1.bounce(collisionVector, player2);
		if(player2.collidesWith(player1, collisionVector))
			player2.bounce(collisionVector, player1);

		if(player1Ammo.collidesWith(player2Ammo,collisionVector))
		{
			//player1Ammo.bounce(collisionVector,player2Ammo);
			player1Ammo.setVelocityX(-1);
			//player1Ammo.setRadians(PI/2);
			player2Ammo.setVelocityX(1);
			player1Ammo.setVelocityY(-50);
			player2Ammo.setVelocityY(1);
		}

		//ammo collides with bacteria
		for(int i = 0; i < bacteria2.getNumBact(); i++)
		{
			//if(player1Ammo.collidesWith(*temp,collisionVector));
			int curBacteria = bacteria2.collisionWAmmo(player1Ammo);
			if((curBacteria != -1) && ammoBool2 && !player1Ammo.getReset())
				//if(bacteriaM.collisionWAmmo(player1Ammo))
			{
				ammoBool2 = false;
				if(sound == true)
					audio->playCue(HIT);
				bacteria2.bacteria[curBacteria]->setInvisible();
				bacteria2.decrementBacteriaLeft();
				player2.setScore(player2.getScore()+100);

				blueAmmo.push_back(nullptr);
				blueAmmo[blueAmmo.size()-1] = new Ammunition1;

				if(blueAmmo.size() > 0)		//function where pass in the index in the array and set up the texture
				{
					if (!blueAmmo[blueAmmo.size()-1]->initialize(this, blueAmmoTM.getWidth(), blueAmmoTM.getHeight(), 0,&blueAmmoTM))
						throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
					blueAmmo[blueAmmo.size()-1]->setPosition(VECTOR2(player1.getX()+player1.getWidth()*player1.getScale(),player1.getY()));
					blueAmmo[blueAmmo.size()-1]->setCollisionType(entityNS::BOX);
					blueAmmo[blueAmmo.size()-1]->setEdge(COLLISION_BOX_PADDLE);
					blueAmmo[blueAmmo.size()-1]->setCollisionRadius(COLLISION_RADIUS);
					blueAmmo[blueAmmo.size()-1]->setScale(1);
				}

				if(player1.getActive())
					player1Ammo.setLaunching();

			}
			//if(bacteriaM.collisionWAmmo(player2Ammo) && ammoBool1)
			curBacteria = bacteria2.collisionWAmmo(player2Ammo);
			if((curBacteria != -1) && ammoBool1 && !player2Ammo.getReset())
				//if(bacteriaM.collisionWAmmo(player1Ammo))
			{
				ammoBool1 = false;
				if(sound == true)
					audio->playCue(HIT);
				player1.setScore(player1.getScore()+100);
				bacteria2.bacteria[curBacteria]->setInvisible();
				bacteria2.decrementBacteriaLeft();
				redAmmo.push_back(nullptr);
				redAmmo[redAmmo.size()-1] = new Ammunition2;
				
				if(redAmmo.size() > 0)		//function where pass in the index in the array and set up the texture
				{
					if (!redAmmo[redAmmo.size()-1]->initialize(this, redAmmoTM.getWidth(), redAmmoTM.getHeight(), 0,&redAmmoTM))
						throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
					redAmmo[redAmmo.size()-1]->setPosition(VECTOR2(player2.getX()-30,player2.getY()));
					redAmmo[redAmmo.size()-1]->setCollisionType(entityNS::BOX);
					redAmmo[redAmmo.size()-1]->setEdge(COLLISION_BOX_PADDLE);
					redAmmo[redAmmo.size()-1]->setCollisionRadius(COLLISION_RADIUS);
					redAmmo[redAmmo.size()-1]->setScale(1);

					if(player2.getActive())
						player2Ammo.setLaunching();
				}

			}
		}

		//ammo collides with virus
		for(int i = 0; i < viruses2.getNumVirus(); i++)
		{
			
			int curVirus = viruses2.collisionWAmmo(player1Ammo);
			if((curVirus != -1) && ammoBool2 && !player1Ammo.getReset())
				//if(bacteriaM.collisionWAmmo(player1Ammo))
			{
				
				//player2.setScore(player2.getScore() + 1);
				ammoBool2 = false;
				if(sound == true)
					audio->playCue(HIT);
				viruses2.viruses[curVirus]->setInvisible();
				viruses2.decrementVirusesLeft();
				player2.setScore(player2.getScore()+100);
				blueAmmo.push_back(nullptr);
				blueAmmo[blueAmmo.size()-1] = new Ammunition1;
				

				if(blueAmmo.size() > 0)		//function where pass in the index in the array and set up the texture
				{
					if (!blueAmmo[blueAmmo.size()-1]->initialize(this, blueAmmoTM.getWidth(), blueAmmoTM.getHeight(), 0,&blueAmmoTM))
						throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
					blueAmmo[blueAmmo.size()-1]->setPosition(VECTOR2(player1.getX()+player1.getWidth()*player1.getScale(),player1.getY()));
					blueAmmo[blueAmmo.size()-1]->setCollisionType(entityNS::BOX);
					blueAmmo[blueAmmo.size()-1]->setEdge(COLLISION_BOX_PADDLE);
					blueAmmo[blueAmmo.size()-1]->setCollisionRadius(COLLISION_RADIUS);
					blueAmmo[blueAmmo.size()-1]->setScale(1);
				}

				if(player1.getActive())
					player1Ammo.setLaunching();

			}
			//if(bacteriaM.collisionWAmmo(player2Ammo) && ammoBool1)
			curVirus = viruses2.collisionWAmmo(player2Ammo);
			if((curVirus != -1) && ammoBool1 && !player2Ammo.getReset())
				//if(bacteriaM.collisionWAmmo(player1Ammo))
			{
				
				ammoBool1 = false;
				if(sound == true)
					audio->playCue(HIT);
				viruses2.viruses[curVirus]->setInvisible();
				viruses2.decrementVirusesLeft();
				player1.setScore(player1.getScore()+100);
				redAmmo.push_back(nullptr);
				redAmmo[redAmmo.size()-1] = new Ammunition2;

				if(redAmmo.size() > 0)		//function where pass in the index in the array and set up the texture
				{
					if (!redAmmo[redAmmo.size()-1]->initialize(this, redAmmoTM.getWidth(), redAmmoTM.getHeight(), 0,&redAmmoTM))
						throw(GameError(gameErrorNS::WARNING, "Paddle not initialized"));
					redAmmo[redAmmo.size()-1]->setPosition(VECTOR2(player2.getX()-30,player2.getY()));
					redAmmo[redAmmo.size()-1]->setCollisionType(entityNS::BOX);
					redAmmo[redAmmo.size()-1]->setEdge(COLLISION_BOX_PADDLE);
					redAmmo[redAmmo.size()-1]->setCollisionRadius(COLLISION_RADIUS);
					redAmmo[redAmmo.size()-1]->setScale(1);

					if(player2.getActive())
						player2Ammo.setLaunching();
				}


			}

			if(player1Ammo.getReset())
			{
				ammoBool2 = true;
			}

			if(player2Ammo.getReset())
			{
				ammoBool1 = true;
			}
		}
		break;
	}
	return;
}

//=============================================================================
// Render game items
//=============================================================================
void CollisionTypes::render()
{
	graphics->spriteBegin();                // begin drawing sprites
	switch (gamestate)
	{
	case splash:
		splashScreen.draw();
		break;
	case menu:
		menuPic.draw();
		gameMenu.displayMenu();
		break;
	case highscore:
		highScorePic.draw();
		break;
	case setting:
		settingPic.draw();
		gameMenu.displaySubMenu(gameMenu.subMen_1Setting,2);
		break;
	case cat:
		catPic.draw();
		break;
	case level1:
		background1.draw();

		for(unsigned int i = 0; i < blueAmmo.size(); i++)
		{
			blueAmmo[i]->draw();
		}
		/*for(unsigned int i = 0; i < blueAmmo.size(); i++)
		{
		
			ammo1Part.draw();
		}*/
		for(unsigned int i = 0; i < redAmmo.size(); i++)
		{
			//redAmmo[i]->draw();
			redAmmo[i]->draw();
		}

		player1.draw();
		if(player1.getActive())
			player1Ammo.draw();
		player2.draw();
		if(player2.getActive())
			player2Ammo.draw();
		bacteria1.render(graphics, player1.getScore(), player2.getScore(), player1, player2);
		killerCell.render(graphics);
		killerCell2.render(graphics);
		eatenParticles.draw();
		//eatenPlayer1Part.draw();
		//eatenPlayer2Part.draw();
		
		//if(player1Ammo.getIsLaunching())
			ammo1Part.draw();
		break;
	case level1Clear:
		level1ClearPic.draw();
		break;
	case level2:
		background2.draw();
		for(unsigned int i = 0; i < blueAmmo.size(); i++)
		{
			blueAmmo[i]->draw();
		}

		for(unsigned int i = 0; i < redAmmo.size(); i++)
		{
			redAmmo[i]->draw();
		}

		player2Ammo.draw();
		player1Ammo.draw();
		player2.draw();
		player1.draw();
		viruses2.render(graphics);
		bacteria2.render(graphics, player1.getScore(), player2.getScore(), player1, player2);
		killerCell.render(graphics);
		killerCell2.render(graphics);
		eatenParticles.draw();
		eatenPlayer1Part.draw();
		eatenPlayer2Part.draw();
		break;
	case level2CheckPoint:
		level2ChkPtPic.draw();
		break;
	case win://blue wins
		losePic.draw();
		gameMenu.displayHighScores(player1.getScore(), player2.getScore());
		break;
	case lose://red wins
		winPic.draw();
		gameMenu.displayHighScores(player1.getScore(), player2.getScore());
		break;
	}
	graphics->spriteEnd();                  // end drawing sprites
	return;
}



//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void CollisionTypes::releaseAll()
{
	player1TM.onLostDevice();
	player2TM.onLostDevice();
	blueAmmoTM.onLostDevice();
	redAmmoTM.onLostDevice();
	splashTM.onLostDevice();
	catTM.onLostDevice();
	menuTM.onLostDevice();
	background1TM.onLostDevice();
	background2TM.onLostDevice();
	winTM.onLostDevice();
	loseTM.onLostDevice();
	level1ClearTM.onLostDevice();
	level2ChkPtTM.onLostDevice();
	settingTM.onLostDevice();
	highScoreTM.onLostDevice();
	output->onLostDevice();
	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void CollisionTypes::resetAll()
{
	player1TM.onResetDevice();
	player2TM.onResetDevice();
	blueAmmoTM.onResetDevice();
	redAmmoTM.onResetDevice();

	splashTM.onResetDevice();
	catTM.onResetDevice();
	menuTM.onResetDevice();
	background1TM.onResetDevice();
	background2TM.onResetDevice();
	winTM.onResetDevice();
	loseTM.onResetDevice();
	level1ClearTM.onResetDevice();
	level2ChkPtTM.onResetDevice();
	settingTM.onResetDevice();
	highScoreTM.onResetDevice();
	output->onResetDevice();
	Game::resetAll();
	return;
}

void CollisionTypes::setAmmoBool1(bool temp)
{
	ammoBool1 = temp;
	return;
}

void CollisionTypes::setAmmoBool2(bool temp)
{
	ammoBool2 = temp;
	return;
}

void CollisionTypes::resetAmmo()
{
	//for(int i = 0; i < bacteria1.getNumBact(); i++)
	//{
	//	bacteria1.bacteria[i]->resetAmmo();
	//}
	eatenBacteria.resize(0);
	eatenVirus.resize(0);
	//eatenVirus = 0;
	//eatenBacteria = 0;
	blueAmmo.resize(0);
	redAmmo.resize(0);
	player1Ammo.setLaunching();
	player2Ammo.setLaunching();
	return;
}

void CollisionTypes::deleteEatenAmmo()
{
	//for(int i = 0; i < blueAmmo.size(); i++)
	//{
	//	if(blueAmmo[i]->getEnemy() == bacteria1.bacteria[eatenBacteria])
	//		blueAmmo[i]->setInvisible();
	//}

	//for(int i = 0; i < redAmmo.size(); i++)
	//{
	//	if(redAmmo[i]->getEnemy() == bacteria1.bacteria[eatenBacteria])
	//		redAmmo[i]->setInvisible();
	//}

	for(int i = 0; i < blueAmmo.size(); i++)
	{
		for(int j = 0; j < eatenBacteria.size(); j++)
		{
			if(blueAmmo[i]->getEnemy() == bacteria1.bacteria[j])
				blueAmmo[i]->setInvisible();
		}
	}

	for(int i = 0; i < redAmmo.size(); i++)
	{
		for(int j = 0; j < eatenBacteria.size(); j++)
		{
			if(redAmmo[i]->getEnemy() == bacteria1.bacteria[j])
				redAmmo[i]->setInvisible();
		}
	}
}
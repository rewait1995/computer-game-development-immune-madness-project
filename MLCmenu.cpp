
#include "MLCmenu.h"

Menu::Menu()
{
	selectedItem = -1;	//nothing return
	menuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
	menuShow = true;
	subMenuShow = false;
	for(int i = 0; i < 7; i++)
	{
		highScore[i] = 0;
	}
}

void Menu::initialize(Graphics *g, Input *i)
{
	menuHeading ="IMMUNE MADNESS";
	mainMen.push_back("PLAY");
	mainMen.push_back("HIGH SCORES");
	mainMen.push_back("SETTINGS");
	mainMen.push_back("SEE MY AWESOME CAT");

	subMen_1Setting.push_back("SOUND");
	subMen_1Setting.push_back("INVINCIBLE");

	highlightColor = graphicsNS::GRAY;
	normalColor = graphicsNS::BLACK;
	activeColor = graphicsNS::RED;

	menuAnchor = D3DXVECTOR2(50,30);
	input = i;
	verticalOffsetMain = 100;
	verticalOffsetSub = 200;
	linePtr = 0;
	selectedItem = -1;
	graphics = g;
	menuItemFont = new TextDX();
	selectedMenuItemFont = new TextDX();
	menuHeadingFont = new TextDX();
	menuItemFontHighlight = new TextDX();
	if(menuItemFont->initialize(graphics, 40, true, false, "Calibri") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(selectedMenuItemFont->initialize(graphics, 40, true, false, "Calibri") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing selectedMenuItem Font"));
	if(menuItemFontHighlight->initialize(graphics, 50, true, false, "Calibri") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuItem font"));
	if(menuHeadingFont->initialize(graphics, 80, true, false, "Calibri") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menuHeading font"));

	menuHeadingFont->setFontColor(normalColor);
	menuItemFont->setFontColor(normalColor);
	menuItemFontHighlight->setFontColor(highlightColor);
	selectedMenuItemFont->setFontColor(activeColor);
	return;
}

void Menu::update(Audio* audio)
{
	if (input->wasKeyPressed(VK_UP))
	{
		if(menuShow)
			linePtr--;
		subLinePtr--;
		audio->playCue(SCROLL);
	}
	if (input->wasKeyPressed(VK_DOWN))
	{
		if(menuShow)
			linePtr++;
		subLinePtr++;
		audio->playCue(SCROLL);
	}
	//line pointer rolls around, could just have it stay there
	if (linePtr > 3) linePtr = 0;
	if(subLinePtr > 1) subLinePtr = 0;
	if (linePtr < 0) linePtr = 3;
	if(subLinePtr > 0) subLinePtr = 1;

	selectedItem = linePtr;

	/*if (input->isKeyDown(VK_RETURN))
	{
		menuShow = false;
		subMenuShow = true;
	}*/
	return;
}

void Menu::displayMenu()
{
	menuHeadingFont->print(menuHeading, (int)menuAnchor.x, (int)menuAnchor.y);
	for(unsigned int i = 0; i < mainMen.size(); i++)
	{
		if(linePtr == i)
			menuItemFontHighlight->print(mainMen[i], (int)menuAnchor.x, (int)menuAnchor.y+(i+1)*verticalOffsetMain);
		else
			menuItemFont->print(mainMen[i], (int)menuAnchor.x, (int)menuAnchor.y+(i+1)*verticalOffsetMain);
	}
	return;
}

void Menu::displaySubMenu(std::vector<std::string> a, int k)
{
	//menuHeadingFont->print(mainMen[k], menuAnchor.x, menuAnchor.y);
	for(int i = 0; i<a.size();i++)
	{
		if (subLinePtr==i)
			menuItemFontHighlight->print(a[i], menuAnchor.x, menuAnchor.y+(i+1)*verticalOffsetSub);
		else if((subLinePtr == i) && (input->wasKeyPressed(VK_RETURN)))
		{
			selectedMenuItemFont->print(a[i], menuAnchor.x, menuAnchor.y+(i+1)*verticalOffsetSub);
		}
		else
			menuItemFont->print(a[i], menuAnchor.x, menuAnchor.y+(i+1)*verticalOffsetSub);
	}
	return;
}

void Menu::displayHighScores(int player1, int player2)
{
	/*file.open("highScores.txt");
	if(file.fail())
		 throw(GameError(gameErrorNS::FATAL_ERROR, "Error opening highScores file"));

	for(int i = 0; i < 7; i++)
	{
		file>>highScore[i];
	}

	insertionSort(highScore, 7);

	for(int i = 0; i < 5; i++)
	{
		file << highScore[i];
	}
	file.close();*/
	menuItemFont->print("Player1 Score: " + std::to_string(player1), menuAnchor.x, menuAnchor.y*verticalOffsetMain);
	menuItemFont->print("Player2 Score: " + std::to_string(player2), menuAnchor.x, menuAnchor.y*verticalOffsetMain*2);
	return;
}

void Menu::insertionSort(int* x, int size)
{
	for(int i = 0; i < size; i++)
	{
		int j = i;
		while ((j > 0)&&(x[j-1] > x[j]))
		{
			swap(x, j, j-1);
			j = j - 1;
		}
	}
	return;
}

void Menu::swap(int* x, int a, int b)
{
	int temp = x[a];
	x[a] = x[b];
	x[b] = temp;
	return;
}
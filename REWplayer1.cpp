// Heavily modified by Michelle Chu Rachel Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
//plasma cell class (player 1)

#include "REWplayer1.h"

//=============================================================================
// default constructor
//=============================================================================
PlasmaPlayer1::PlasmaPlayer1() : Entity()
{
    spriteData.width = plasmaPlayer1::WIDTH;           // size of Ship1
    spriteData.height = plasmaPlayer1::HEIGHT;
    spriteData.x = plasmaPlayer1::X;                   // location on screen
    spriteData.y = plasmaPlayer1::Y;
    spriteData.rect.bottom = plasmaPlayer1::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = plasmaPlayer1::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    radius = plasmaPlayer1::WIDTH*2;
	collisionType = entityNS::CIRCLE;
	eaten = false;
	score = 0;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void PlasmaPlayer1::update(float frameTime)
{
    Entity::update(frameTime);
    //spriteData.angle += frameTime * plasmaPlayer::ROTATION_RATE;  // rotate the ship

	int directionX = 0;
	int directionY = 0;

	if((input->isKeyDown(VK_RIGHT)))
		directionX++;
	if((input->isKeyDown(VK_LEFT)))
		directionX--;
	if((input->isKeyDown(VK_UP)))
		directionY--;
	if((input->isKeyDown(VK_DOWN)))
		directionY++;

	setX(getX() + (plasmaPlayer1::SPEED*directionX*frameTime));
	setY(getY() + (plasmaPlayer1::SPEED*directionY*frameTime));
	if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-plasmaPlayer1::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-plasmaPlayer1::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-plasmaPlayer1::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-plasmaPlayer1::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
}

void PlasmaPlayer1::setScore(int a)
{
	score = a;
	return;
}

void PlasmaPlayer1::setVelocityX(float a)
{
	velocity.x = a;
}

void PlasmaPlayer1::setVelocityY(float a)
{
	velocity.y = a;
}

void PlasmaPlayer1::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void PlasmaPlayer1::setVisible()
{
	Image::setVisible(true);
	active = true;
}

void PlasmaPlayer1::setEaten(bool a)
{
	eaten = a;
	return;
}
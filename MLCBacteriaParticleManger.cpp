#include "MLCBacteriaParticleManger.h"

BacteriaParticleManager::BacteriaParticleManager()
{
	srand(time(NULL));
	//for (int i = 0; i < MAX_NUMBER_ENDOTOXIN_PARTICLES; i++)
	//{
	//	float timeAlive;
	//	float maxTimeAlive;
	//	float fadeValue; //1: opaque, 0: transparent
	//	float scaleValue;
	//	float rotationValue;
	//}
}
BacteriaParticleManager::~BacteriaParticleManager()
{
}
float BacteriaParticleManager::getVariance()
{
	float foo = (rand() );
	foo = ((int)foo	% 500);
	foo += 2.0f;
	return foo;
}
void BacteriaParticleManager::setInvisibleAllParticles()
{
	for (int i = 0; i < MAX_NUMBER_ENDOTOXIN_PARTICLES; i++)
	{
		particles[i].setVisible(false);
		particles[i].setActive(false);
	}
	return;
}
void BacteriaParticleManager::setVisibleNParticles(int n)
{
	if (n == 0) return;
	int activatedParticles = 0;
	for (int i = 0; i < MAX_NUMBER_ENDOTOXIN_PARTICLES; i++)
	{
		if (!particles[i].getActive()) //found an inactive particle
		{
			particles[i].setActive(true);
			particles[i].setMaxTimeAlive(MAX_ENDOTOXIN_PARTICLE_LIFETIME);
			float newX = velocity.x * getVariance()*1.5f; 
			float newY = velocity.y  * getVariance();
			VECTOR2 v = VECTOR2(newX,newY);
			particles[i].setX(position.x);
			particles[i].setY(position.y);
			particles[i].setVelocity(v);
			particles[i].setVisible(true);
			activatedParticles++;
			if (activatedParticles >= n)
				return;
		}
	}
	return;
}

bool BacteriaParticleManager::initialize(Graphics *g)
{
	if (!tm.initialize(g, ENDOTOXIN_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dust texture"));
	for (int i = 0; i < MAX_NUMBER_EATEN_PARTICLES; i++)
	{
		if (!particles[i].initialize(g,0,0,0,&tm))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing dust"));
		particles[i].setActive(false);
		particles[i].setVisible(false);
		particles[i].setScale(0.3f);
		particles[i].setRotationValue(0.9f);
	}
	return true;
}

void BacteriaParticleManager::update(float frametime)
{
	for (int i = 0; i < MAX_NUMBER_ENDOTOXIN_PARTICLES; i++){
		if (particles[i].getActive())
			particles[i].update(frametime);
	}
	return;
}

void BacteriaParticleManager::draw()
{
	byte fadeAmount;
	COLOR_ARGB color;
	for (int i = 0; i < MAX_NUMBER_ENDOTOXIN_PARTICLES; i++)
	{
		if (!particles[i].getActive())
			continue;
		float foo = particles[i].getMaxTimeAlive();  //MAX_PARTICLE_LIFETIME;
		float bar = particles[i].getTimeAlive();
		float foobar = (foo-bar)/foo;
		fadeAmount = 255 * foobar;
		color = D3DCOLOR_ARGB(fadeAmount,0xff,0xff,0xff);//fadeAmount,fadeAmount,fadeAmount);
		particles[i].draw(color);
		/*if (fadeAmount <= 10)
			particles[i].resetParticle();*/
		
	}
	return;
}

void BacteriaParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles)
{
	setPosition(pos);
	setVelocity(vel);
	setVisibleNParticles(numParticles);
	return;
}
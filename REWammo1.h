// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 ship.h v1.0

#define WIN32_LEAN_AND_MEAN
#ifndef _REWAMMO1_H                 // Prevent multiple definitions if this 
#define _REWAMMO1_H                // file is included in more than one place
class Ammunition1;

#include "entity.h"
#include "constants.h"
#include "REWplayer1.h"
#include "MLCbacterium.h"
#include "MLCbacteriumManagerh.h"
#include <vector>
#include "MLCAmmo1ParticleManger.h"
#include "audio.h"


//#include "collisionTypes.h"

namespace ammunition1
{
    const int WIDTH = 75;                   // image width
    const int HEIGHT = 75;                  // image height
    const int X = GAME_WIDTH/7 - WIDTH/7;   // location on screen
    const int Y = GAME_HEIGHT - HEIGHT;
    const float ROTATION_RATE = (float)PI/3; // radians per second
    const float SPEED = 250;                // 100 pixels per second
    const float MASS = 300.0f;              // mass
    const int   TEXTURE_COLS = 1;           // texture has 2 columns
}

// inherits from Entity class
class Ammunition1 : public Entity
{
private:
	bool isLaunching;
	int frameCounter;
	int curving;
	bool isFirstFrame;
	bool isReset;
	int enemyFollow;
	Entity* curEnemy;
	VECTOR2 location;
	float attachedRadians;
	//Ammo1ParticleManager ammoPart;

public:
    // constructor
    Ammunition1();
	//void initialize(Graphics* graphics, Game* game, int a);

	virtual ~Ammunition1();

    // inherited member functions
    void update(float frameTime, PlasmaPlayer1 player1, int index, Audio*);
	void initializeAmmo1(Graphics* g);
	void shoot(float frameTime);

	void setVelocityX(int);
	void setVelocityY(int);

	void setInvisible();

	void setVisible();
	//BacteriumManager bacteriaM;
	//PlasmaPlayer2* player2ptr;
	//CollisionTypes tempCT;

	bool getReset();
	void setLaunching();
	void setEnemy(Entity*);
	void setLocation(VECTOR2);
	void drawAmmoPart();

	bool getIsLaunching() {return isLaunching;}
	Entity* getEnemy() {return curEnemy;}

	//make setter for viruses
};
#endif


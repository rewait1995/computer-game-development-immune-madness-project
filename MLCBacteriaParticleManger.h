#ifndef _BACTERIA_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _BACTERIA_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"
#include <stdlib.h>
#include <time.h>
#include "graphics.h"


class BacteriaParticleManager
{
	private:
	Particle particles[MAX_NUMBER_ENDOTOXIN_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float BacteriaParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	BacteriaParticleManager();
	~BacteriaParticleManager();
	void BacteriaParticleManager::setInvisibleAllParticles();
	void BacteriaParticleManager::setVisibleNParticles(int n);
	void BacteriaParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void BacteriaParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool BacteriaParticleManager::initialize(Graphics *g);

	void BacteriaParticleManager::update(float frametime);
	void BacteriaParticleManager::draw();
	void BacteriaParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
#define WIN32_LEAN_AND_MEAN
#ifndef MLC_KILLER_T_CELL_MANAGER
#define MLC_KILLER_T_CELL_MANAGER

#include "MLCkillerTCell.h"
#include <cmath>
#include <stdlib.h>
#include <time.h>

class TCellManager : killerTCell
{
private:
	TextureManager pacTM;
	killerTCell pac;
	Entity targetEntity;
	int playerEaten;

public:
	TCellManager();
	~TCellManager();
	void initializeTCellandTM(Graphics* graphics, Game* game);
	void setAnimationInitial();
	void animateLeftI();
	void animateLeftM();
	void animateRightI();
	void animateRightM();
	void animateUpI();
	void animateUpM();
	void animateDownI();
	void animateDownM();
	void update(float frameTime);
	void render(Graphics* graphics);
	void ai(float time, Entity &t);
	void deltaTrack(float frameTime);
	killerTCell getKillerCell();
	int getPlayerEaten();
	bool collision(Entity t);
	void reset();
	bool firstPlayerEaten;

};
#endif
// Heavily modified by Michelle Chu Rachel Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
//plasma cell class (player 1)

#include "REWplasmaPlayer.h"

//=============================================================================
// default constructor
//=============================================================================
PlasmaPlayer::PlasmaPlayer() : Entity()
{
    spriteData.width = plasmaPlayer::WIDTH;           // size of Ship1
    spriteData.height = plasmaPlayer::HEIGHT;
    spriteData.x = plasmaPlayer::X;                   // location on screen
    spriteData.y = plasmaPlayer::Y;
    spriteData.rect.bottom = plasmaPlayer::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = plasmaPlayer::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    radius = plasmaPlayer::WIDTH/2.0;
    collisionType = entityNS::CIRCLE;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void PlasmaPlayer::update(float frameTime)
{
    Entity::update(frameTime);
    //spriteData.angle += frameTime * plasmaPlayer::ROTATION_RATE;  // rotate the ship

	int directionX = 0;
	int directionY = 0;

	if((input->isKeyDown(VK_RIGHT)))
		directionX++;
	if((input->isKeyDown(VK_LEFT)))
		directionX--;
	if((input->isKeyDown(VK_UP)))
		directionY--;
	if((input->isKeyDown(VK_DOWN)))
		directionY++;

	setX(getX() + (plasmaPlayer::SPEED*directionX*frameTime));
	setY(getY() + (plasmaPlayer::SPEED*directionY*frameTime));
	if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-plasmaPlayer::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-plasmaPlayer::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-plasmaPlayer::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-plasmaPlayer::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
}

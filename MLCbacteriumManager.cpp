#include "MLCbacteriumManagerh.h"

BacteriumManager::BacteriumManager()
{
	minutes = 0;
	seconds = 0;
}

BacteriumManager::~BacteriumManager()
{
	bacteriaTM.onLostDevice();
	bacteriaTM.onResetDevice();
	for(int i = 0; i < getNumBact(); i++)
	{
		SAFE_DELETE(bacteria[i]);
	}
}

void BacteriumManager::initialize(Graphics* graphics, Game* game, int a)
{
	if (!bacteriaTM.initialize(graphics, BACTERIUM_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bacteria texture"));
	numBact = a;
	bacterialeft = numBact;
	//pushing back appropriate length of pointers
	for(int i = 0; i < numBact; i++)
	{
		bacteria.push_back(nullptr);
	}
	//initiating array of unactivated caspase pointers
	for(int i = 0; i < numBact; i++)
	{
		bacteria[i] = new Bacterium;
	}
	for(int i = 0; i < getNumBact(); i++)
	{
		if(!bacteria[i]->initialize(game, bacteriumNS::WIDTH, bacteriumNS::HEIGHT, bacteriumNS::TEXTURE_COLS, &bacteriaTM))
			throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing bacteria texture"));
		bacteria[i]->setX((float)GAME_WIDTH/(rand()%10+1));
		bacteria[i]->setY((float)GAME_HEIGHT/(rand()%10+1));
		bacteria[i]->setVisible();
		bacteria[i]->setVelocity(VECTOR2((rand()%5+1)*bacteriumNS::SPEED_X, (rand()%5+1)*-bacteriumNS::SPEED_Y));
		//bacteria[i]->setCollisionType(entityNS::CIRCLE);
		//bacteria[i]->setEdge(COLLISION_BOX_BACTERIA);
		//bacteria[i]->setCollisionRadius(COLLISION_RADIUS_BACTERIA);
		//bacteria[i]->setScale(1);
		bacteria[i]->setFrames(BACT_WIGGLE_START, BACT_WIGGLE_END);   // animation frames
		bacteria[i]->setCurrentFrame(BACT_WIGGLE_START);     // starting frame
		bacteria[i]->setFrameDelay(BACT_ANIMATION_DELAY);
	}
	bactpart.initialize(graphics);
	output = new TextDX();
	if(output->initialize(graphics, 15, true, false, "Arial") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing output font"));

	timer = new TextDX();
	if(timer->initialize(graphics, 60, true, false, "Arial") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing output font"));

	noteBacktomenu = new TextDX();
	if(noteBacktomenu->initialize(graphics, 30, true, false, "Arial") == false)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing output font"));
	return;
}
	
void BacteriumManager::update(float frameTime)
{
	VECTOR2 pos, vel;

	seconds += frameTime;

	if(seconds >= 60)
	{
		minutes++;
		seconds = 0;
	}
	for(int i = 0; i < getNumBact(); i++)
	{
		if(bacteria[i]->getVisible())
		{
			pos = bacteria[i]->getCenterPoint();
			vel = VECTOR2(-.05,-.05);
			bactpart.createParticleEffect(pos, vel, 1);
			bactpart.update(frameTime);
			bacteria[i]->update(frameTime);
		}
	}
	return;
}

int BacteriumManager::collision(Entity t, int a)
{
	VECTOR2 collisionVector, pos1, vel1, pos2, vel2;
    // if collision between two bacteria or killer cell
	for(int i = 0; i < getNumBact(); i++)
	{
		if((bacteria[i]->collidesWith(t, collisionVector)) && (bacteria[i]->getVisible()) && (a == -1))
		{
			bacteria[i]->bounce(collisionVector, t);
			//bacteria[i]->reverseDirection();
		}
		if((bacteria[i]->collidesWith(t, collisionVector)) && (bacteria[i]->getVisible()) && (a==1))
		{
			bacteria[i]->bounce(collisionVector, t);
			t.bounce(collisionVector, *bacteria[i]);
			//bacteria[i]->reverseDirection();
		}
		//bacteria colliding with each other
		for(int j = 0; j < getNumBact(); j++)
		{
			if(bacteria[i]->collidesWith(*bacteria[j], collisionVector))
			{
				// bacteria bounce off each other
				bacteria[i]->bounce(collisionVector, *bacteria[j]);
				bacteria[j]->bounce(collisionVector*-1, *bacteria[i]);
				/*pos1 = bacteria[i]->getCenterPoint();
				vel1 = VECTOR2((bacteria[i]->getVelocity())/100);
				pos2 = bacteria[j]->getCenterPoint();
				vel2 = VECTOR2((bacteria[j]->getVelocity())/100);
				bactpart.createParticleEffect(pos1, vel1, 10);
				bactpart.createParticleEffect(pos2, vel2, 10);*/

				/*bacteria[i]->reverseDirection();
				bacteria[j]->reverseDirection();*/
			}
		}
	}
	return 0;
}

int BacteriumManager::collisionWAmmo(Entity t)
{
	VECTOR2 collisionVector;
	for(int i = 0; i < getNumBact(); i++)
	{
		if(bacteria[i]->collidesWith(t, collisionVector))
		{
			return i;
		}
	}
	return -1;
}
void BacteriumManager::render(Graphics* graphics, int redScore, int blueScore, PlasmaPlayer1 player1, PlasmaPlayer2 player2)
{
	graphics->spriteBegin();                // begin drawing sprites
	/*bactpart.setInvisibleAllParticles();*/
		for(int i = 0; i < getNumBact(); i++)
	{
		if(bacteria[i]->getVisible())
		{
			bactpart.draw();
		}
	}
	for(int i = 0; i < getNumBact(); i++)
	{
		if(bacteria[i]->getVisible())
		{
			bacteria[i]->draw();
		}
	}

	timer->setFontColor(graphicsNS::WHITE);
	if(seconds < 10)
	{
		timer->print(std::to_string(minutes) + ":0" + std::to_string((int)seconds),1200,50);
	}
	else
	{
		timer->print(std::to_string(minutes) + ":" + std::to_string((int)seconds),1200,50);
	}

	noteBacktomenu->setFontColor(graphicsNS::PURPLE);

	if((minutes == 3) && (seconds > 45))
		noteBacktomenu->print("Go back to the menu at any time using BACKSPACE",100,50);

	if(!player2.isEaten())
		output->print("Score:" + std::to_string(redScore), (int)player2.getX()+40, (int)player2.getY());
	if(!player1.isEaten())
		output->print("Score:" + std::to_string(blueScore), (int)player1.getX()+40,(int)player1.getY());
	/*Bacterium *temp;
	for(int i = 0; i < getNumBact(); i++)
	{
		temp = bacteria[i];
		output->print("red:" + std::to_string(temp->getPink()), temp->getX(),temp->getY());
		output->print("blue:" + std::to_string(temp->getBlue()), temp->getX(),temp->getY()+10);
	}*/
    graphics->spriteEnd();    
	return;
}
	
void BacteriumManager::ai(float time, Entity &t, int sightDistance)
{
	targetEntity = t;
	evade(sightDistance);
	return;
}

void BacteriumManager::evade(int sightDistance)
{
	//VECTOR2 vel = getCenterPoint() - targetEntity.getCenterPoint();
	//float length = D3DXVec2Length(&vel);
	//if(length < sightDistance)
	//{
	//	VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
	//	setVelocity(vel);
	//	return;
	//}
	//setVelocity(D3DXVECTOR2 (0,0));//attack mechanic

	for(int i = 0; i < getNumBact(); i++)
	{
		VECTOR2 vel = bacteria[i]->getCenterPoint() - targetEntity.getCenterPoint();
		float length = D3DXVec2Length(&vel);
		if (length < sightDistance)
		{
			//VECTOR2* foo = D3DXVec2Normalize(&vel, &vel);
			bacteria[i]->setVelocity(vel);
		}
	}
	return;
}

int BacteriumManager::getBacteriaLeft()
{
	return bacterialeft;
}

void BacteriumManager::decrementBacteriaLeft()
{
	bacterialeft--;
	return;
}

void BacteriumManager::reset()
{
	bacterialeft = numBact;
	for(int i = 0; i < getNumBact(); i++)
	{
		bacteria[i]->setVisible();
	}
	return;
}
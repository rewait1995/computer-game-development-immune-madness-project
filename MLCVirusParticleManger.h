#ifndef _VIRUS_PARTICLE_MANAGER_H                // Prevent multiple definitions if this 
#define _VIRUS_PARTICLE_MANAGER_H                // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "particle.h"
#include "constants.h"
#include "textureManager.h"
#include <stdlib.h>
#include <time.h>
#include "graphics.h"


class VirusParticleManager
{
	private:
	Particle particles[MAX_NUMBER_ENDOTOXIN_PARTICLES];
	VECTOR2 velocity; //all particles created using SetVisibleNParticles() will use this velocity
	VECTOR2 position; //all particles created using SetVisibleNParticles() will use this position
	TextureManager tm;

	float VirusParticleManager::getVariance();// returns a number between 50% and 150% for particle variance

public:
	VirusParticleManager();
	~VirusParticleManager();
	void VirusParticleManager::setInvisibleAllParticles();
	void VirusParticleManager::setVisibleNParticles(int n);
	void VirusParticleManager::setPosition(VECTOR2 pos) {position = pos;}
	void VirusParticleManager::setVelocity(VECTOR2 vel) {velocity = vel;}
	bool VirusParticleManager::initialize(Graphics *g);

	void VirusParticleManager::update(float frametime);
	void VirusParticleManager::draw();
	void VirusParticleManager::createParticleEffect(VECTOR2 pos, VECTOR2 vel, int numParticles);
};
#endif
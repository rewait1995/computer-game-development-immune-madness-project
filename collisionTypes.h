// Programming 2D Games
// Copyright (c) 2011,2012 by: 
// Charles Kelly
// collisionTypes.h v1.0

#ifndef _COLLISION_TYPES_H      // Prevent multiple definitions if this 
#define _COLLISION_TYPES_H      // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

class CollisionTypes;

#include "game.h"
#include "graphics.h"
#include "textDX.h"
#include "textureManager.h"
#include "image.h"
#include <cmath>
#include "paddle.h"
#include "puck.h"
#include "bricks.h"
#include "patternStep.h"
#include "REWplayer1.h"
#include "REWplayer2.h"
#include "REWammo1.h"
#include "REWammo2.h"
#include "MLCkillerTCellManager.h"
#include "MLCvirusManager.h"
#include "MLCbacteriumManagerh.h"
#include "MLCmenu.h"
#include "MLCEatenParticleManager.h"
#include "MLCAmmo1ParticleManger.h"
#include "MLCEatenBacteriaParticleManger .h"
#include "MLCEatenVirusParticleManger.h"

#define maxPatternSteps 4

//=============================================================================
// This class is the core of the game
//=============================================================================
class CollisionTypes : public Game
{
private:
    // game items
    TextureManager paddleTM, puckTM, brickTM, player1TM, player2TM, blueAmmoTM, 
		redAmmoTM, background1TM, background2TM, catTM, menuTM, winTM, loseTM, level1ClearTM, level2ChkPtTM, splashTM,
		highScoreTM, settingTM;// game texture
	Image background1, background2, catPic, menuPic, splashScreen, winPic, losePic, level1ClearPic, level2ChkPtPic,
		highScorePic, settingPic;
    VECTOR2 collisionVector;    // collision vector
	Paddle paddle;
	Puck puck;
	PlasmaPlayer1 player1;
	PlasmaPlayer2 player2;
	Ammunition1	  player1Ammo;
	std::vector<Ammunition1*> blueAmmo;
	std::vector<Ammunition2*> redAmmo;
	Ammunition2	  player2Ammo;
	Ammo1ParticleManager ammo1Part;
	EatenVirusParticleManager eatenPlayer1Part;
	EatenBacteriaParticleManager eatenPlayer2Part;

	VECTOR2 locationVec;

	TCellManager killerCell;
	TCellManager killerCell2;

	VirusManager viruses2;
	BacteriumManager bacteria1;
	BacteriumManager bacteria2;
	int score;
	bool collision;
	Brick bricks;	
	EatenParticleManager eatenParticles;
	PatternStep patternSteps[maxPatternSteps];
	int patternStepIndex;
	GameStates gamestate;
	Menu gameMenu;
	float timeInState;
	int frameCounter;
	//GameStates lastGameState;
	/*int redScore;
	int blueScore;*/

	bool ammoBool1;
	bool ammoBool2;
	TextDX *output;
	bool leaveMenu;
	bool resetLevel1Pics;
	bool resetLevel1Params;
	bool resetLevel2Pics;
	bool resetLevel2Params;
	bool checkPointThrough;
	bool chaseBaddie;
	bool sound;
	bool invincible;
	bool chaseBactOnly;
	bool level2MusicOn;
	int playerEaten;
	int player1ScoreCHK;
	int player2ScoreCHK;
	int minutes;
	float seconds;

	//Graphics* graphics

public:
    // Constructor
    CollisionTypes();

    // Destructor
    virtual ~CollisionTypes();
	// Delete all reserved memory.
    virtual void deleteAll();
	bool level1Initialize;
	bool level2Initialize;
    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
    void releaseAll();
    void resetAll();
	void resetAmmo();
	void gameStateUpdate();
	void deleteEatenAmmo();
	std::vector<int> eatenBacteria;
	std::vector<int> eatenVirus;

	/*int eatenBacteria;
	int eatenVirus;*/

	void setAmmoBool1(bool);
	void setAmmoBool2(bool);
};

#endif

#include "MLCgameVars.h"

GameVars::GameVars()
{
	level = 1;
}
GameVars::GameVars(int b, int v)
{
	setNumBact(b);
	setNumVirus(v);
	level = 1;
}
int GameVars::getNumBact()
{
	return numBacteria;
}
void GameVars::setNumBact(int a)
{
	numBacteria = a;
}
int GameVars::getNumVirus()
{
	return numVirus;
}
void GameVars::setNumVirus(int a)
{
	numVirus = a;
}
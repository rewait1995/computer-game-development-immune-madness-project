// Heavily modified by Michelle Chu Rachel Rachel Waitverlech from Programming 2D Games
// Copyright (c) 2011 by: Charles Kelly
//plasma cell class (player 2)

#include "REWplayer2.h"

//=============================================================================
// default constructor
//=============================================================================
PlasmaPlayer2::PlasmaPlayer2() : Entity()
{
    spriteData.width = plasmaPlayer2::WIDTH;           // size of Ship1
    spriteData.height = plasmaPlayer2::HEIGHT;
    spriteData.x = plasmaPlayer2::X;                   // location on screen
    spriteData.y = plasmaPlayer2::Y;
    spriteData.rect.bottom = plasmaPlayer2::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = plasmaPlayer2::WIDTH;
    velocity.x = 0;                             // velocity X
    velocity.y = 0;                             // velocity Y
    radius = plasmaPlayer2::WIDTH*2;
    collisionType = entityNS::CIRCLE;
	eaten = false;
	score = 0;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void PlasmaPlayer2::update(float frameTime)
{
    Entity::update(frameTime);
    //spriteData.angle += frameTime * plasmaPlayer::ROTATION_RATE;  // rotate the ship

	int directionX = 0;
	int directionY = 0;

	if((input->isKeyDown(PLAYER2_RIGHT)))
		directionX++;
	if((input->isKeyDown(PLAYER2_LEFT)))
		directionX--;
	if((input->isKeyDown(PLAYER2_UP)))
		directionY--;
	if((input->isKeyDown(PLAYER2_DOWN)))
		directionY++;

	setX(getX() + (plasmaPlayer2::SPEED*directionX*frameTime));
	setY(getY() + (plasmaPlayer2::SPEED*directionY*frameTime));
	if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-plasmaPlayer2::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-plasmaPlayer2::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-plasmaPlayer2::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-plasmaPlayer2::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
}

void PlasmaPlayer2::setScore(int a)
{
	score = a;
	return;
}

void PlasmaPlayer2::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void PlasmaPlayer2::setVisible()
{
	Image::setVisible(true);
	active = true;
}

void PlasmaPlayer2::setEaten(bool a)
{
	eaten = a;
	return;
}
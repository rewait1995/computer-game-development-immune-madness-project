
              // file is included in more than one place
#define WIN32_LEAN_AND_MEAN
#ifndef MENU_H                 // Prevent multiple definitions if this 
#define MENU_H 

class Menu;

#include "graphics.h"
#include "constants.h"
#include "textDX.h"
#include <string>
#include <sstream>
#include "input.h"
#include <vector>
#include <iostream>
#include <fstream>
#include <iomanip>
#include "audio.h"

namespace menuNS
{ }

// inherits from Entity class
class Menu 
{
private:
   TextDX *menuItemFont;
   TextDX *menuItemFontHighlight;
   TextDX *menuHeadingFont;
   TextDX *selectedMenuItemFont;
   Input   *input;         // pointer to the input system
  // TextDX *output;
   //std::string outString;
   Graphics *graphics;
   
   std::string menuHeading;
   std::string menuItem1;
   std::string menuItem2;
   std::string menuItem3;
   D3DXVECTOR2 menuAnchor;
   int verticalOffsetMain;
   int verticalOffsetSub;

  std::fstream file;
  int highScore[7];

   COLOR_ARGB highlightColor;
   COLOR_ARGB normalColor;
   COLOR_ARGB activeColor;

   void insertionSort(int* x, int size);
   void swap(int* x, int a, int b);
   

public:
    // constructor
    Menu();
	void initialize(Graphics *g, Input *i);
	void update(Audio* audio);
	int getSelectedItem() {return selectedItem;}
	void displayMenu();
	std::string getText(std::vector<std::string>,int i);
	void setText(std::vector<std::string>,std::string);
	void displaySubMenu(std::vector<std::string> a, int k);
	void displayHighScores(int player1, int player2);

	
	std::vector<std::string> mainMen;
    std::vector<std::string> subMen_1Setting;

	int selectedItem;
	int linePtr;
	bool menuShow;
	bool subMenuShow;
	int subLinePtr;
};
#endif


// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 6 constants.h v1.1

#ifndef _CONSTANTS_H            // Prevent multiple definitions if this 
#define _CONSTANTS_H            // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include <windows.h>

//=============================================================================
// Function templates for safely dealing with pointer referenced items.
// The functions defined by these templates may be called using a normal
// function call syntax. The compiler will create a function that replaces T
// with the type of the calling parameter.
//=============================================================================
// Safely release pointer referenced item
template <typename T>
inline void safeRelease(T& ptr)
{
    if (ptr)
    { 
        ptr->Release(); 
        ptr = NULL;
    }
}
#define SAFE_RELEASE safeRelease            // for backward compatiblility

// Safely delete pointer referenced item
template <typename T>
inline void safeDelete(T& ptr)
{
    if (ptr)
    { 
        delete ptr; 
        ptr = NULL;
    }
}
#define SAFE_DELETE safeDelete              // for backward compatiblility

// Safely delete pointer referenced array
template <typename T>
inline void safeDeleteArray(T& ptr)
{
    if (ptr)
    { 
        delete[] ptr; 
        ptr = NULL;
    }
}
#define SAFE_DELETE_ARRAY safeDeleteArray   // for backward compatiblility

// Safely call onLostDevice
template <typename T>
inline void safeOnLostDevice(T& ptr)
{
    if (ptr)
        ptr->onLostDevice(); 
}
#define SAFE_ON_LOST_DEVICE safeOnLostDevice    // for backward compatiblility

// Safely call onResetDevice
template <typename T>
inline void safeOnResetDevice(T& ptr)
{
    if (ptr)
        ptr->onResetDevice(); 
}
#define SAFE_ON_RESET_DEVICE safeOnResetDevice  // for backward compatiblility

//=============================================================================
//                  Constants
//=============================================================================

// window
const char CLASS_NAME[] = "Immune Madness";
const char GAME_TITLE[] = "Immune Madness";
const bool FULLSCREEN = false;              // windowed or fullscreen
const UINT GAME_WIDTH =  1350;               // width of game in pixels
const UINT GAME_HEIGHT = 680;               // height of game in pixels
const int NUM_FRAMES = 400;
const int AMMO_VELOCITY = 350;

// game
const double PI = 3.14159265;
const float FRAME_RATE = 200.0f;                // the target frame rate (frames/sec)
const float MIN_FRAME_RATE = 10.0f;             // the minimum frame rate
const float MIN_FRAME_TIME = 1.0f/FRAME_RATE;   // minimum desired time for 1 frame
const float MAX_FRAME_TIME = 1.0f/MIN_FRAME_RATE; // maximum time used in calculations
const RECT  COLLISION_RECTANGLE = {-30,-16,30,16};
const RECT  COLLISION_BOX_PADDLE = {-45,-10,45,10};
const RECT COLLISION_BOX_PUCK = {-32, -32, 32, 32};
const RECT COLLISION_BOX_BACTERIA = {-120, -120, 120, 120};
const RECT COLLISION_BOX_VIRUS = {-118, 120, -118, 120};
const RECT COLLISION_BOX_TCELL = {-120, -120, 120, 120};
const float COLLISION_RADIUS = 29;
const float COLLISION_RADIUS_BACTERIA = 60;
const float COLLISION_RADIUS_TCELL = 60;

const int MAX_BAC_ANTIGEN = 5;

//

// graphic images
const char PLAYER1_IMAGE[] = "pictures\\REW-blue animation.png";
//const char PLAYER2_IMAGE[] = "pictures\\red b cell.png";
const char PLAYER2_IMAGE[] = "pictures\\REW-red animation.png";
const char PLAYER1_AMMO_IMAGE[] = "pictures\\blue ammo.png";
const char PLAYER2_AMMO_IMAGE[] = "pictures\\red ammo.png";
const char TCELL_IMAGE[] = "pictures\\MLCKillerCell_MoveIdle_Animation.png";
const char BACTERIUM_IMAGE[] = "pictures\\MLC_bacteria_animation_sheet.png";
const char VIRUS_IMAGE[] = "pictures\\MLC_virus_animation.png";
const char BACKGROUND1_IMAGE[] = "pictures\\MLCbackground.png";
const char BACKGROUND2_IMAGE[] = "pictures\\MLCbackgroundLevel2.jpg";
const char CAT_IMAGE[] = "pictures\\MLCcat.png";
const char MENU_IMAGE[] = "pictures\\MLCmenu.png";
const char WIN_IMAGE[] = "pictures\\MLCLoseScreen.jpg";
const char LOSE_IMAGE[] = "pictures\\MLCWinScreen.jpg";
const char SPLASH_SCREEN[] = "pictures\\splash.png";
const char LEVEL1_DONE_IMAGE[] = "pictures\\MLCLevel1Clear.jpg";
const char LEVEL2_CHKPOINT_IMAGE[] = "pictures\\MLCLevel2ChkPt.jpg";
const char HIGHSCORES_IMAGE[] = "pictures\\highScoresFillIn.jpg";
const char SETTINGS_IMAGE[] = "pictures\\MLCsettings.jpg";
const char DNAPOLY_IMAGE[] = "pictures\\MLCdnaPolyParticle.png";
const char ENDOTOXIN_IMAGE[] = "pictures\\MLCBactPart.png";
const char VIRALTOXIN_IMAGE[]= "pictures\\MLCVirusPart.png";
const char EATEN_BACT_IMAGE[] = "pictures\\MLCbacteriumPart.png";
const char EATEN_VIRUS_IMAGE[] = "pictures\\MLCVirusPart.png";

const float TCELL_ANIMATION_DELAY = 0.2f;    // time between frames of ship animation
const float BACT_ANIMATION_DELAY = 0.5f;
const float VIRUS_ANIMATION_DELAY = 0.5f;
const float P1_ANIMATION_DELAY = 0.2f;
const float P2_ANIMATION_DELAY = 0.2f;

//Virus Animation
const int VIRUS_COLS = 3;
const int VIRUS_WIDTH = 128;
const int VIRUS_HEIGHT = 128;

//Virus Actions
const int VIRUS_WIGGLE_START = 0;
const int VIRUS_WIGGLE_END = 2;

//Bacteria Animation
const int BACT_COLS = 3;
const int BACT_WIDTH = 128;
const int BACT_HEIGHT = 128;

//Bacteria Actions
const int BACT_WIGGLE_START = 0;
const int BACT_WIGGLE_END = 2;

//T Cell Cell Animations
const int  TCELL_COLS = 4;
const int  TCELL_WIDTH = 128;
const int  TCELL_HEIGHT = 128;

//TCELL actions
const int TCELL_IDLE_LEFT_START = 0;			//1st row
const int TCELL_IDLE_LEFT_END = 3;
const int TCELL_MOVE_LEFT_START = 4;				//2nd row
const int TCELL_MOVE_LEFT_END = 7;
const int TCELL_IDLE_RIGHT_START = 8;
const int TCELL_IDLE_RIGHT_END = 11;
const int TCELL_MOVE_RIGHT_START = 12;
const int TCELL_MOVE_RIGHT_END = 15;
const int TCELL_IDLE_UP_START = 16;
const int TCELL_IDLE_UP_END = 19;
const int TCELL_MOVE_UP_START = 20;
const int TCELL_MOVE_UP_END = 23;
const int TCELL_IDLE_DOWN_START = 24;
const int TCELL_IDLE_DOWN_END = 27;
const int TCELL_MOVE_DOWN_START = 28;
const int TCELL_MOVE_DOWN_END = 31;

//Player 1 Animation
const int P1_COLS = 4;
const int P1_WIDTH = 128;
const int P1_HEIGHT = 128;

//Player 1 Actions
const int P1_ANIMATE_START = 0;
const int P1_ANIMATE_END = 3;

//Player 2 Animation
const int P2_COLS = 4;
const int P2_WIDTH = 128;
const int P2_HEIGHT = 128;

//Player 2 Actions
const int P2_ANIMATE_START = 0;
const int P2_ANIMATE_END = 3;

// key mappings
// In this game simple constants are used for key mappings. If variables were used
// it would be possible to save and restore key mappings from a data file.
const UCHAR ESC_KEY      = VK_ESCAPE;       // escape key
const UCHAR ALT_KEY      = VK_MENU;         // Alt key
const UCHAR	PLAYER2_LEFT    = 0x41;
const UCHAR PLAYER2_RIGHT   = 0x44;		
const UCHAR	PLAYER2_UP    = 0x57;
const UCHAR PLAYER2_DOWN   = 0x53;	
const UCHAR PADDLE_RIGHT   = VK_RIGHT;    // right arrow
const UCHAR ENTER_KEY    = VK_RETURN; 
const UCHAR PADDLE_UP = VK_UP;
const UCHAR PADDLE_DOWN = VK_DOWN;
//

// audio files required by audio.cpp
// WAVE_BANK must be location of .xwb file.
const char WAVE_BANK[]  = "audio\\Win\\Wave Bank.xwb";
// SOUND_BANK must be location of .xsb file.
const char SOUND_BANK[] = "audio\\Win\\Sound Bank.xsb";

// MLC Sounds
const char BACKGROUND[] = "playful";
const char HIT[] = "hit";
const char THROW[] = "throw";
const char EAT[] = "eat";
const char LEVEL2[] = "level2";
const char INVINCIBLE[] = "invincible";
const char SCROLL[] = "scroll";
const char SELECT[] = "select";
const char BACK[] = "back";
const char WIN[] = "cheer";
const char LOSE[] = "Sad Trombone";

//particles
const float MAX_EATEN_PARTICLE_LIFETIME = 1.5f;
const int MAX_NUMBER_EATEN_PARTICLES = 10000;

const float MAX_ENDOTOXIN_PARTICLE_LIFETIME = 5.0f;
const int MAX_NUMBER_ENDOTOXIN_PARTICLES = 10000;

const float MAX_VIRALTOXIN_PARTICLE_LIFETIME = 1.0f;
const int MAX_NUMBER_VIRALTOXIN_PARTICLES = 10000;

const float MAX_AMMO_PARTICLE_LIFETIME = 0.5f;
const int MAX_NUMBER_AMMO_PARTICLES = 10000;

// Pattern Step Action
enum PATTERN_STEP_ACTION {NONE, UP, DOWN, LEFT, RIGHT, TRACK, EVADE};

//GameStates
enum GameStates {splash, menu, highscore, setting, cat, level1, level1Clear, level2, level2CheckPoint, win, lose};

#endif

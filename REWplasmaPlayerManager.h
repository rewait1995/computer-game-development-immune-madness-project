#define WIN32_LEAN_AND_MEAN
#ifndef REW_PLAYER_MANAGER
#define REW_PLAYER_MANAGER

#include "REWplayer1.h"
#include "REWplayer2.h"
#include "entity.h"
#include "textDX.h"
#include <vector>

class plasmaPlayerManager : public Entity
{
private:
	TextDX *output;
	int redScore;
	int blueScore;
	PlasmaPlayer1 player1;
	PlasmaPlayer2 player2;
public:
	plasmaPlayerManager(PlasmaPlayer1, PlasmaPlayer2);
	~plasmaPlayerManager();
	void initialize(Graphics* graphics, Game* game);
	void collision(Entity t);
	void update(float frameTime);
	void render(Graphics* graphics);
	void ai(float time, Entity &t);
};

#endif
#define WIN32_LEAN_AND_MEAN
#ifndef MLC_BACTERIUM_H
#define MLC_BACTERIUM_H

class Bacterium;

#include "entity.h"
#include "constants.h"
#include "graphics.h"

namespace bacteriumNS
{
    const int WIDTH = 128;                   // image width
    const int HEIGHT = 128;                  // image height
    const int X = GAME_WIDTH/2 - WIDTH/2;   // location on screen
    const int Y = GAME_HEIGHT/2 - HEIGHT/2;
    const float SPEED_X = 100;               
	const float SPEED_Y = 100;             // mass
	const int TEXTURE_COLS = 3;
 
}

// inherits from Entity class
class Bacterium : public Entity
{
private:
   // puckNS::DIRECTION direction;    
    bool collision;                 
    bool target;  
	int directionX;
	int directionY;
	VECTOR2 velocity;
	float speed;
	Entity targetEntity;
	int antigen;
	int pink;
	int blue;
	//Game* game;

public:
    // constructor
    Bacterium();

    // inherited member functions
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);

    // Set collision Boolean
    void setCollision(bool c)
    {collision = c;}

    // Set collision type (NONE, CIRCLE, BOX, ROTATED_BOX)
    virtual void setCollisionType(entityNS::COLLISION_TYPE ctype)
    {collisionType = ctype;}

    // Set RECT structure used for BOX and ROTATED_BOX collision detection.
    void setEdge(RECT e) {edge = e;}

    // Set target
    void setTarget(bool t) {target = t;}

    // Get collision
    bool getCollision() {return collision;}

    // Get collision type
    entityNS::COLLISION_TYPE getCollisionType() {return collisionType;}

	void setInvisible();

	void setVisible();

	void setVelocity(VECTOR2 v) {velocity = v;}

	VECTOR2 getVelocity() {return velocity;}

	bool isCovered();

	int getPink() {return pink;}
	void incrementPink();
	int getBlue() {return blue;}
	void incrementBlue();
	void clearVars();
	int getNumCovered();

	//void reverseDirection(float frameTime);
};
#endif
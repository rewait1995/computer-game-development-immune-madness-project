#include "MLCkillerTCellManager.h"

TCellManager::TCellManager()
{
	srand(time(NULL));
	playerEaten = 0;
}

TCellManager::~TCellManager()
{
	pacTM.onLostDevice();
	pacTM.onResetDevice();
}

void TCellManager::initializeTCellandTM(Graphics* graphics, Game* game)
{
	int foo = (rand());
	int bar = (rand());
	if (!pacTM.initialize(graphics, TCELL_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing TCell texture"));
	if (!pac.initialize(game, TCELL_WIDTH, TCELL_HEIGHT, TCELL_COLS,&pacTM))
		throw(GameError(gameErrorNS::WARNING, "TCell not initialized"));
	pac.setPosition(VECTOR2((rand()%GAME_WIDTH)+(GAME_WIDTH), (rand()%GAME_HEIGHT)+(GAME_HEIGHT/2)));
	//pac.setCollisionType(entityNS::CIRCLE);
    //pac.setEdge(COLLISION_BOX_TCELL);
	//pac.setCollisionRadius(COLLISION_RADIUS_TCELL);
	pac.setScale(1.5);
	return;
}

void TCellManager::setAnimationInitial()
{
	pac.setFrames(TCELL_MOVE_UP_START, TCELL_MOVE_UP_END);   // animation frames
    pac.setCurrentFrame(TCELL_MOVE_UP_START);     // starting frame
    pac.setFrameDelay(TCELL_ANIMATION_DELAY);
	return;
}

void TCellManager::animateLeftI()
{
	pac.setFrames(TCELL_IDLE_LEFT_START, TCELL_IDLE_LEFT_END);
	return;
}
void TCellManager::animateLeftM()
{
	pac.setFrames(TCELL_MOVE_LEFT_START, TCELL_MOVE_LEFT_END);
	return;
}
void TCellManager::animateRightI()
{
	pac.setFrames(TCELL_IDLE_RIGHT_START, TCELL_IDLE_RIGHT_END);
	return;
}
void TCellManager::animateRightM()
{
	pac.setFrames(TCELL_MOVE_RIGHT_START, TCELL_MOVE_RIGHT_END);
	return;
}
void TCellManager::animateUpI()
{
	pac.setFrames(TCELL_IDLE_UP_START, TCELL_IDLE_UP_END);
	return;
}
void TCellManager::animateUpM()
{
	pac.setFrames(TCELL_MOVE_UP_START, TCELL_MOVE_UP_END);
	return;
}
void TCellManager::animateDownI()
{
	pac.setFrames(TCELL_IDLE_DOWN_START, TCELL_IDLE_DOWN_END);
	return;
}
void TCellManager::animateDownM()
{
	pac.setFrames(TCELL_MOVE_DOWN_START, TCELL_MOVE_DOWN_END);
	return;
}

void TCellManager::render(Graphics* graphics)
{
	graphics->spriteBegin();                // begin drawing sprites
	pac.draw();
    graphics->spriteEnd();    
	return;
}

void TCellManager::update(float frameTime)
{
	pac.update(frameTime);
}

void TCellManager::ai(float time, Entity &t)
{
	//if players have equal numbers enemies hit, T cell doesn't move
	//if one player is winning, the T cell goes after him
	//if foreign body has full antibody complement, T cell goes after it
	targetEntity = t;
	deltaTrack(time);
	return;
}
void TCellManager::deltaTrack(float frameTime)
{
	VECTOR2 vel = D3DXVECTOR2(1,1);
	VECTOR2 targetCenter = targetEntity.getCenterPoint();
	if((abs(pac.getCenterPoint().x - targetCenter.x) < killerTCellNS::WIDTH)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.y = 1;
		vel.x = 0;
		animateDownM();
	}

	////2 special corner cases
	//else if((pac.getCenterPoint().x - targetCenter.x) <= (2*killerTCellNS::WIDTH*1.5))
	//{
	//	vel.x = -1;
	//	vel.y = 0;
	//	animateLeftM();
	//}
	//else if((pac.getPositionX()+(pac.getWidth()*pac.getScale()) <= (targetEntity.getPositionX()-10)) 
	//	&& (targetEntity.getPositionX() >= (GAME_WIDTH-targetEntity.getWidth()-10)))
	//{
	//	vel.x= 1;
	//	vel.y = 0;
	//	animateRightM();
	//}

	else if((abs(pac.getCenterPoint().x - targetCenter.x) < killerTCellNS::WIDTH)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.y = -1;
		vel.x = 0;
		animateUpM();
	}
	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(abs(pac.getCenterPoint().y - targetCenter.y) < killerTCellNS::HEIGHT))
	{
		vel.x = -1;
		vel.y = 0;
		animateLeftM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(abs(pac.getCenterPoint().y - targetCenter.y) < killerTCellNS::HEIGHT))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}

	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 0;
		vel.y = -1;
		animateUpM();
	}

	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}
	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y == targetCenter.y))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}
	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y == targetCenter.y))
	{
		vel.x = -1;
		vel.y = 0;
		animateLeftM();
	}
	else if((pac.getCenterPoint().x == targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}
	else if((pac.getCenterPoint().x == targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 0;
		vel.y = -1;
		animateUpM();
	}
	else if((pac.getCenterPoint().x == targetCenter.x)&&
			 (pac.getCenterPoint().y == targetCenter.y))
	{ 
		//set idle to prev. direction or something
		vel.x = 0;
		vel.y = 0;
	}

	if((abs(pac.getCenterPoint().x - targetCenter.x) < killerTCellNS::WIDTH)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.y = 1;
		vel.x = 0;
		animateDownM();
	}
	else if((abs(pac.getCenterPoint().x - targetCenter.x) < killerTCellNS::WIDTH)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.y = -1;
		vel.x = 0;
		animateUpM();
	}
	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(abs(pac.getCenterPoint().y - targetCenter.y) < killerTCellNS::HEIGHT))
	{
		vel.x = -1;
		vel.y = 0;
		animateLeftM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(abs(pac.getCenterPoint().y - targetCenter.y) < killerTCellNS::HEIGHT))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}

	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 0;
		vel.y = -1;
		animateUpM();
	}

	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}

	if((abs(pac.getCenterPoint().x - targetCenter.x) < killerTCellNS::WIDTH)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.y = 1;
		vel.x = 0;
		animateDownM();
	}
	else if((abs(pac.getCenterPoint().x - targetCenter.x) < killerTCellNS::WIDTH)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.y = -1;
		vel.x = 0;
		animateUpM();
	}
	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(abs(pac.getCenterPoint().y - targetCenter.y) < killerTCellNS::HEIGHT))
	{
		vel.x = -1;
		vel.y = 0;
		animateLeftM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(abs(pac.getCenterPoint().y - targetCenter.y) < killerTCellNS::HEIGHT))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}

	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 0;
		vel.y = -1;
		animateUpM();
	}

	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}

	else if((pac.getCenterPoint().x < targetCenter.x)&&
		(pac.getCenterPoint().y == targetCenter.y))
	{
		vel.x = 1;
		vel.y = 0;
		animateRightM();
	}
	else if((pac.getCenterPoint().x > targetCenter.x)&&
		(pac.getCenterPoint().y == targetCenter.y))
	{
		vel.x = -1;
		vel.y = 0;
		animateLeftM();
	}
	else if((pac.getCenterPoint().x == targetCenter.x)&&
		(pac.getCenterPoint().y < targetCenter.y))
	{
		vel.x = 0;
		vel.y = 1;
		animateDownM();
	}
	else if((pac.getCenterPoint().x == targetCenter.x)&&
		(pac.getCenterPoint().y > targetCenter.y))
	{
		vel.x = 0;
		vel.y = -1;
		animateUpM();
	}

	//Trying different tracking method, didn't work
	//if((abs(pac.getPositionX() - targetEntity.getPositionX()) < killerTCellNS::WIDTH)&&
	//	(pac.getPositionY() < targetEntity.getPositionX()))
	//{
	//	vel.y = 1;
	//	vel.x = 0;
	//	animateDownM();
	//}
	//else if((abs(pac.getPositionX() -targetEntity.getPositionX()) < killerTCellNS::WIDTH)&&
	//	(pac.getPositionY() > targetEntity.getPositionY()))
	//{
	//	vel.y = -1;
	//	vel.x = 0;
	//	animateUpM();
	//}
	//else if((pac.getPositionX() > targetEntity.getPositionX())&&
	//	(abs(pac.getPositionY() - targetEntity.getPositionY()) < killerTCellNS::HEIGHT))
	//{
	//	vel.x = -1;
	//	vel.y = 0;
	//	animateLeftM();
	//}

	//else if((pac.getPositionX() < targetEntity.getPositionX())&&
	//	(abs(pac.getPositionY() - targetEntity.getPositionY()) < killerTCellNS::HEIGHT))
	//{
	//	vel.x = 1;
	//	vel.y = 0;
	//	animateRightM();
	//}

	//else if((pac.getPositionX() < targetEntity.getPositionX())&&
	//	(pac.getPositionY() > targetEntity.getPositionY()))
	//{
	//	vel.x = 1;
	//	vel.y = 0;
	//	animateRightM();
	//}

	//else if((pac.getPositionX() > targetEntity.getPositionX())&&
	//	(pac.getPositionY() > targetEntity.getPositionY()))
	//{
	//	vel.x = 0;
	//	vel.y = -1;
	//	animateUpM();
	//}

	//else if((pac.getPositionX() > targetEntity.getPositionX())&&
	//	(pac.getPositionY() < targetEntity.getPositionY()))
	//{
	//	vel.x = 0;
	//	vel.y = 1;
	//	animateDownM();
	//}

	//else if((pac.getPositionX() < targetEntity.getPositionX())&&
	//	(pac.getPositionY() < targetEntity.getPositionY()))
	//{
	//	vel.x = 0;
	//	vel.y = 1;
	//	animateDownM();
	//}
	//else if((pac.getPositionX() < targetEntity.getPositionX())&&
	//	(pac.getPositionY() == targetEntity.getPositionY()))
	//{
	//	vel.x = 1;
	//	vel.y = 0;
	//	animateRightM();
	//}
	//else if((pac.getPositionX() > targetEntity.getPositionX())&&
	//	(pac.getPositionY() == targetEntity.getPositionY()))
	//{
	//	vel.x = -1;
	//	vel.y = 0;
	//	animateLeftM();
	//}
	//else if((pac.getPositionX() == targetEntity.getPositionX())&&
	//	(pac.getPositionY() < targetEntity.getPositionY()))
	//{
	//	vel.x = 0;
	//	vel.y = 1;
	//	animateDownM();
	//}
	//else if((pac.getPositionX() == targetEntity.getPositionX())&&
	//	(pac.getPositionY() > targetEntity.getPositionY()))
	//{
	//	vel.x = 0;
	//	vel.y = -1;
	//	animateUpM();
	//}
	//else if((pac.getPositionX() == targetEntity.getPositionX())&&
	//		 (pac.getPositionY() == targetEntity.getPositionY()))
	//{ 
	//	//set idle to prev. direction or something
	//	vel.x = 0;
	//	vel.y = 0;
	//}
	VECTOR2* foo = D3DXVec2Normalize(&vel,&vel);
	pac.setVelocity(vel);
}

killerTCell TCellManager::getKillerCell()
{
	return pac;
}
int TCellManager::getPlayerEaten()
{
	return playerEaten;
}

bool TCellManager::collision(Entity t)
{
	VECTOR2 collisionVector;
    // if collision between killer and someone (player)
	if(pac.collidesWith(t, collisionVector))
	{
		playerEaten++;
		return true;
	}
	return false;
}
void TCellManager::reset()
{
	playerEaten = 0;
	pac.setPosition(VECTOR2((rand()%GAME_WIDTH)+(GAME_WIDTH/2), (rand()%GAME_HEIGHT)+(GAME_HEIGHT/2)));
	return;
}
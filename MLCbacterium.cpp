#include "MLCbacterium.h"

//=============================================================================
// default constructor
//=============================================================================
Bacterium::Bacterium() : Entity()
{
    spriteData.width = bacteriumNS::WIDTH;           
    spriteData.height = bacteriumNS::HEIGHT;
    spriteData.x = bacteriumNS::X;                   // location on screen
    spriteData.y = bacteriumNS::Y;
	//was HEIGHT/2 for some reason?
    //spriteData.rect.bottom = bacteriumNS::HEIGHT;    // rectangle to select parts of an image
    //spriteData.rect.right = bacteriumNS::WIDTH;

	velocity = D3DXVECTOR2(1,1);
    startFrame = 0;              // first frame of ship animation
    endFrame     = 0;              // last frame of ship animation
    currentFrame = startFrame;
    radius = 55;                 // collision radius
    collision = false;
    collisionType =entityNS::CIRCLE;// entityNS::CIRCLE;
    target = false;
	//edge.bottom = -bacteriumNS::HEIGHT/2;
	spriteData.scale = 1;
	active = true;
	speed = 150;
	setMass(300);
	speed = 50;
	setMass(10);
	antigen = 2;
	pink = 0;
	blue = 0;
}

bool Bacterium::initialize(Game *gamePtr, int width, int height, int ncols,
    TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Bacterium::setInvisible()
{
	Image::setVisible(false);
	active = false;
}

void Bacterium::setVisible()
{
	Image::setVisible(true);
	active = true;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Bacterium::update(float frameTime)
{
	Entity::update(frameTime);

	//Code for wrapping, bacteria end up moving in superpredictable patterns just boring up and down 
	/*if(getX() >= (GAME_WIDTH)-(getWidth()*getScale()))
	{
		setX(0);
	}
	if(getX() <= 0)
	{
		setX((GAME_WIDTH)-(getWidth()*getScale()));
	}
	if(getY() >= GAME_HEIGHT)
	{
		setY(0);
	}
	if(getY() <= 0)
	{
		setY((GAME_HEIGHT)-(getHeight()*getScale()));
	}*/
	setX(getX() + velocity.x*frameTime);
	setY(getY() + velocity.y*frameTime);
    // Bounce off walls
    // if hit right screen edge
    if (spriteData.x > GAME_WIDTH-bacteriumNS::WIDTH*getScale())
    {
        // position at right screen edge
        spriteData.x = GAME_WIDTH-bacteriumNS::WIDTH*getScale();
        velocity.x = -velocity.x;               // reverse X direction
    } 
    else if (spriteData.x < 0)                  // else if hit left screen edge
    {
        spriteData.x = 0;                       // position at left screen edge
        velocity.x = -velocity.x;               // reverse X direction
    }
    // if hit bottom screen edge
    if (spriteData.y > GAME_HEIGHT-bacteriumNS::HEIGHT*getScale())
    {
        // position at bottom screen edge
        spriteData.y = GAME_HEIGHT-bacteriumNS::HEIGHT*getScale();
        velocity.y = -velocity.y;               // reverse Y direction
    }
    else if (spriteData.y < 0)                  // else if hit top screen edge
    {
        spriteData.y = 0;                       // position at top screen edge
        velocity.y = -velocity.y;               // reverse Y direction
    }
	/*Entity::update(frameTime);
	VECTOR2 foo = velocity*frameTime*speed;
	if (getPositionX() + Image::getWidth()*Image::getScale() > GAME_WIDTH)
	{
		setPosition(D3DXVECTOR2(0,getPositionY()));
	}
	if (getPositionX() < 0)
	{
		setPosition(D3DXVECTOR2(GAME_WIDTH-Image::getWidth()*Image::getScale(),getPositionY()));
	}
	if (getPositionY() + Image::getHeight()*Image::getScale() > GAME_HEIGHT)
	{
		setPosition(D3DXVECTOR2(getPositionX(),0));
	}
	if (getPositionY() < 0)
	{
		setPosition(D3DXVECTOR2(getPositionX(),GAME_WIDTH-Image::getHeight()*Image::getScale()));
	}

	velocity = D3DXVECTOR2(0,0);
	incPosition(foo);
	Image::setX(getPositionX());
	Image::setY(getPositionY());*/
}

bool Bacterium::isCovered()
{
	if((antigen-pink-blue) == 0)
		return true;
	else
		return false;
}

void Bacterium::incrementPink()
{
	pink++;
	return;
}

void Bacterium::incrementBlue()
{
	blue++;
	return;
}

int Bacterium::getNumCovered()
{
	return (pink+blue);
}
void Bacterium::clearVars()
{
	int pink = 0;
	int blue = 0;
	return;
}

//void Bacterium::reverseDirection(float frameTime)
//{
//	setX(getX() + velocity.x*frameTime);
//	setY(getY() + velocity.y*frameTime);
//	spriteData.y = getPositionY();
//	spriteData.x = getPositionX();
//	velocity.y = -velocity.y;
//	return;
//}
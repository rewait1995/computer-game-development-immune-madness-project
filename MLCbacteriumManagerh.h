#define WIN32_LEAN_AND_MEAN
#ifndef MLC_BACTERIUM_MANAGER
#define MLC_BACTERIUM_MANAGER

#include "MLCbacterium.h"
#include "MLCgameVars.h"
#include "REWplayer1.h"
#include "REWplayer2.h"
#include "textDX.h"
#include <vector>
#include "MLCBacteriaParticleManger.h"

class BacteriumManager : Bacterium
{
private:
	TextureManager bacteriaTM;
	Entity targetEntity;
	int bacterialeft;
	TextDX *output, *timer, *noteBacktomenu;
	int numBact;
	BacteriaParticleManager bactpart;
public:
	BacteriumManager();
	~BacteriumManager();
	void initialize(Graphics* graphics, Game* game, int a);
	int collision(Entity t, int a);
	int collisionWAmmo(Entity t);
	void update(float frameTime);
	void render(Graphics* graphics, int, int, PlasmaPlayer1, PlasmaPlayer2);
	void ai(float time, Entity &t, int sightDistance);
	void evade(int sightDistance);
	int getBacteriaLeft();
	void decrementBacteriaLeft();
	int getNumBact() {return numBact;}
	void reset();
	std::vector<Bacterium*> bacteria;

	int minutes;
	float seconds;
};

#endif